import { readFile, writeFile } from 'fs/promises';
import path from 'path';
import { exec } from 'child_process';

const DISABLE_MAX_CLASSES_PER_FILE = `eslint-disable-next-line max-classes-per-file`;

const AST_FOLDER_PATH = path.resolve(__dirname, '..', 'src', 'ast');
const AST_DEFINITION_FILE = path.resolve(AST_FOLDER_PATH, 'ast.def');
const AST_TS_FILE = path.resolve(AST_FOLDER_PATH, 'nodes.ts');
const AST_KIND_FILE = path.resolve(AST_FOLDER_PATH, 'NodeKind.ts');
const AST_GUARD_FILE = path.resolve(AST_FOLDER_PATH, 'guards.ts');
const AST_INDEX_FILE = path.resolve(AST_FOLDER_PATH, 'index.ts');
const BASE_NODE_NAME = 'Node';

const DEFINITION_NODE_PREFIX = ':node';

interface AstNodeField {
  readonly name: string;
  readonly type: string;
}

interface AstNodeDefinition {
  readonly name: string;
  readonly kind: string;
  readonly fields: AstNodeField[];
}

/**
 * Parses a `.def` file and returns an array of AstNodeDefinitions.
 *
 * A `.def` file respects the following format:
 * ```ts
 * :node NodeName
 *   NodeKind
 *   FieldName Type
 *   FieldName Type
 * ```
 */
const parseDefinitionFile = async () => {
  const content = await readFile(AST_DEFINITION_FILE, { encoding: 'utf-8' });
  const nodes = content.split(DEFINITION_NODE_PREFIX).filter(e => !!e);

  const defs = Promise.all(
    nodes.map(async node => {
      const lines = node
        .split('\n')
        .filter(l => !!l)
        .map(l => l.trim());

      const [name, kind, ...fieldDefs] = lines;
      const fields = fieldDefs.map(field => {
        const [fieldName, type] = field.split(':');

        return {
          name: fieldName.trim(),
          type: type.trim(),
        } as AstNodeField;
      });

      return {
        name: name.trim(),
        kind: kind.trim(),
        fields,
      } as AstNodeDefinition;
    }),
  );

  return defs;
};

const writeKindsFile = async (kinds: string[]) => {
  const kindType = `export type AstNodeKind = ${kinds
    .map(kind => `'${kind}'`)
    .join(' | ')};`;

  const kindContents = kinds
    .map(kind => `const ${kind}: AstNodeKind = '${kind}';`)
    .join('\n');

  const exports = `export const AstNodeKind = { ${kinds
    .map(kind => `${kind}`)
    .join(',\n')} }`;

  await writeFile(
    AST_KIND_FILE,
    `${kindType}\n\n${kindContents}\n\n${exports}`,
  );
};

const writeNodesFile = async (defs: AstNodeDefinition[]) => {
  const nodes = defs
    .map(def =>
      `
    export class ${def.name}${BASE_NODE_NAME} extends ${BASE_NODE_NAME} {
      public constructor(
        ${def.fields
          .map(field => `public readonly ${field.name}: ${field.type}`)
          .join(',\n  ')},
        token: Token
      ) {
        super(AstNodeKind.${def.kind}, token);
      }

      public visit<Return>(visitor: IVisitor<Return>): Return {
        return visitor.visit${def.name}${BASE_NODE_NAME}(this);
      }
    }
  `.trim(),
    )
    .join('\n\n');

  const visitor = `
export interface IVisitor<Return> {
  ${defs
    .map(
      def =>
        `visit${def.name}${BASE_NODE_NAME}(node: ${def.name}${BASE_NODE_NAME}): Return;`,
    )
    .join('\n  ')}
    interpretFunction(node: BlockNode, environment: Environment<Return>): Return;
}
  `.trim();

  const baseNode = `
export abstract class ${BASE_NODE_NAME} {
  public readonly kind: AstNodeKind;
  public readonly token: Token;

  public constructor(kind: AstNodeKind, token: Token) {
    this.kind = kind;
    this.token = token;
  }

  public abstract visit<Return>(visitor: IVisitor<Return>): Return;
}
  `.trim();

  const imports = `
import { AstNodeKind } from './NodeKind';
import { Option } from '../utils/types/Option';
import { Token } from 'src/tokens';
import { Environment } from 'src/runtime/Environment';
  `.trim();

  const content = `
/* ${DISABLE_MAX_CLASSES_PER_FILE} */
${imports}

${visitor}

${baseNode}

${nodes}
  `.trim();

  await writeFile(AST_TS_FILE, content);
};

const writeGuardFile = async (defs: AstNodeDefinition[]) => {
  const guards = defs
    .map(def =>
      `
const is${def.name}${BASE_NODE_NAME} = (node: Node): node is ${def.name}${BASE_NODE_NAME} => {
  return node.kind === AstNodeKind.${def.kind};
};
  `.trim(),
    )
    .join('\n\n');

  const content = `
import { AstNodeKind } from './NodeKind';
import { Node, ${defs
    .map(def => `${def.name}${BASE_NODE_NAME}`)
    .join(', ')} } from './nodes';

${guards}

export const AstGuards = {
  ${defs.map(def => `is${def.name}${BASE_NODE_NAME}`).join(',\n')}
};
  `.trim();

  await writeFile(AST_GUARD_FILE, content);
};

const writeIndexFile = async () => {
  const content = `
export * from './NodeKind';
export * from './nodes';
export { AstGuards } from './guards';
  `.trim();

  await writeFile(AST_INDEX_FILE, content);
};

const prettifyFiles = () => {
  exec(`yarn format`, (err, stdout, stderr) => {
    if (err) {
      console.error(err);
    }

    if (stderr) {
      console.error(stderr);
    }

    if (stdout) {
      console.log(stdout);
    }

    console.log('Prettified files');
  });
};

const main = async (argv: string[]) => {
  if (argv.length > 0) {
    // TODO: handle from command line data
    // return;
    throw new Error(
      'Creating nodes from the command line is not supported yet',
    );
  }

  const defs = await parseDefinitionFile();
  const kinds = defs.map(def => def.kind);

  await writeKindsFile(kinds);
  await writeNodesFile(defs);
  await writeGuardFile(defs);
  await writeIndexFile();

  prettifyFiles();
};

main(process.argv.splice(2));
