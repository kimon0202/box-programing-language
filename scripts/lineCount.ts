import { readdir, readFile } from 'fs/promises';
import path from 'path';

const SRC_DIR = path.resolve(__dirname, '..', 'src');
const SCRIPTS_DIR = __dirname;
const TESTS_DIR = path.resolve(__dirname, '..', 'tests');

const DIRS = [SRC_DIR, SCRIPTS_DIR, TESTS_DIR];

const readDir = async (dir: string): Promise<string[]> => {
  const content = await readdir(dir, {
    withFileTypes: true,
    encoding: 'utf-8',
  });

  const files = content
    .filter(c => c.isFile())
    .map(f => path.join(dir, f.name));

  const dirs = content
    .filter(c => c.isDirectory())
    .map(d => path.join(dir, d.name));

  const subDirFiles = (
    await Promise.all([...dirs.map(d => readDir(d))])
  ).flat();
  const allFiles = [...files, ...subDirFiles];

  return allFiles;
};

const main = async () => {
  const files = (await Promise.all([...DIRS.map(readDir)])).flat();

  const info = await Promise.all(
    files.map(async filePath => {
      const content = await readFile(filePath, 'utf-8');

      const lines = content.split('\n');
      const actualCodeLines = lines.filter(l => l.trim() !== '');

      return [lines.length, actualCodeLines.length] as [number, number];
    }),
  );

  const fileCount = files.length;
  const lineCount = info.reduce((prev, acc) => prev + acc[0], 0);
  const codeLineCount = info.reduce((prev, acc) => prev + acc[1], 0);

  console.log(
    `${fileCount} files, ${lineCount} lines, ${codeLineCount} code lines`,
  );
};

main();
