/* eslint-disable no-continue */
import { readFile, readdir } from 'fs/promises';
import path from 'path';
import { inspect } from 'util';
import { execSync } from 'child_process';

const TESTS_DIR = path.resolve(__dirname, '..', 'examples');
const TEST_FILE_EXTENSION = '.box';
const TEST_DESCRIPTOR_EXTENSION = '.txt';

const ERROR_INDENTATION = '[ERROR] '.length;
const INFO_INDENTATION = '[INFO] '.length;

const STDIN_LABEL = ':stdin';
const STDOUT_LABEL = ':stdout';
const STDERR_LABEL = ':stderr';
const RETURNCODE_LABEL = ':returncode';

// eslint-disable-next-line no-shadow
enum DescriptorParsingState {
  None,
  STDIN,
  STDOUT,
  STDERR,
  RETURNCODE,
}

interface TestInfo {
  readonly name: string;

  readonly file: string;
  readonly descriptor: string;

  wasSuccessful: boolean;
}

interface DescriptorInfo {
  readonly name: string;
  readonly stdin: string;
  readonly stdout: string;
  readonly stderr: string;
  readonly returnCode: number;
}

const readDir = async (dir: string): Promise<string[]> => {
  const content = await readdir(dir, {
    withFileTypes: true,
    encoding: 'utf-8',
  });

  const files = content
    .filter(c => c.isFile())
    .map(f => path.join(dir, f.name));

  const dirs = content
    .filter(c => c.isDirectory())
    .map(d => path.join(dir, d.name));

  const subDirFiles = (
    await Promise.all([...dirs.map(d => readDir(d))])
  ).flat();
  const allFiles = [...files, ...subDirFiles];

  return allFiles;
};

const log = (...items: unknown[]): void => {
  const toLog = items.map(i => inspect(i, { depth: Infinity, colors: true }));
  console.log(...toLog);
};

const info = (message: string): void => {
  const finalMessage = [`\x1b[34m[INFO]\x1b[0m `, message].join('');
  console.info(finalMessage);
};

const error = (message: string): void => {
  const finalMessage = [`\x1b[31m[ERROR]\x1b[0m `, message].join('');
  console.error(finalMessage);
};

const success = (message: string): void => {
  const finalMessage = [`\x1b[32m[SUCCESS]\x1b[0m `, message].join('');
  console.log(finalMessage);
};

const indent = (message: string, indentation = 0): string => {
  const prefix = ' '.repeat(indentation);
  return [prefix, message].join('');
};

const checkTests = (tests: string[], descriptors: string[]) => {
  if (tests.length !== descriptors.length) {
    error(
      [
        'Number of tests and descriptors are not the same.',
        indent(`Tests: ${tests.length}`, ERROR_INDENTATION),
        indent(`Descriptors: ${descriptors.length}`, ERROR_INDENTATION),
      ].join('\n'),
    );

    const testsMissingDescriptor = tests.filter(
      t =>
        !descriptors.includes(
          t.replace(TEST_FILE_EXTENSION, TEST_DESCRIPTOR_EXTENSION),
        ),
    );

    testsMissingDescriptor.forEach(t => {
      error(`Missing descriptor: ${t}`);
    });

    return false;
  }

  return true;
};

const createTestInfo = async (tests: [string, string][]): Promise<TestInfo[]> =>
  Promise.all(
    tests.map(async ([filePath, descriptorPath]) => {
      const descriptor = await readFile(descriptorPath, { encoding: 'utf-8' });
      const name = descriptor.split('\n')[0];

      return {
        name,
        file: filePath,
        descriptor,
        wasSuccessful: false,
      };
    }),
  );

const extractDescriptorInfo = (descriptor: string): DescriptorInfo => {
  const lines = descriptor.split('\n').filter(l => !!l);

  let parsingState = DescriptorParsingState.None;

  const stdin: string[] = [];
  const stdout: string[] = [];
  const stderr: string[] = [];
  let returnCode = 0;

  for (let i = 0; i < lines.length; i++) {
    const line = lines[i];

    if (line === STDIN_LABEL) {
      parsingState = DescriptorParsingState.STDIN;
      continue;
    }

    if (line === STDOUT_LABEL) {
      parsingState = DescriptorParsingState.STDOUT;
      continue;
    }

    if (line === STDERR_LABEL) {
      parsingState = DescriptorParsingState.STDERR;
      continue;
    }

    if (line === RETURNCODE_LABEL) {
      parsingState = DescriptorParsingState.RETURNCODE;
      continue;
    }

    if (parsingState === DescriptorParsingState.STDIN) {
      stdin.push(line);
      continue;
    }

    if (parsingState === DescriptorParsingState.STDOUT) {
      stdout.push(line);
      continue;
    }

    if (parsingState === DescriptorParsingState.STDERR) {
      stderr.push(line);
      continue;
    }

    if (parsingState === DescriptorParsingState.RETURNCODE) {
      returnCode = parseInt(line, 10);
      continue;
    }
  }

  return {
    name: lines[0],
    stdin: stdin.join('\n'),
    stdout: stdout.join('\n'),
    stderr: stderr.join('\n'),
    returnCode,
  };
};

const runTest = async (test: TestInfo): Promise<TestInfo> => {
  info(`Running test: \`${test.name}\` - ${test.file}`);

  const descriptorInfo = extractDescriptorInfo(test.descriptor);

  let wasSuccessful = false;

  try {
    const stdout = execSync(`yarn start ${test.file}`);

    if (!stdout.includes(descriptorInfo.stdout)) {
      error(
        [
          `Test ${test.name} failed. Reason:`,
          indent(
            `The stdout did not match the expected content`,
            ERROR_INDENTATION,
          ),
        ].join('\n'),
      );

      console.log('\n');
      console.log([`Expected:`, descriptorInfo.stdout].join('\n'));
      console.log('\n');
      console.log([`Actual:`, stdout].join('\n'));

      wasSuccessful = false;
    } else {
      success(`Test \`${test.name}\` was successful`);
      wasSuccessful = true;
    }
  } catch (err) {
    error(
      [
        `Test ${test.name} failed. Reason:`,
        indent(JSON.stringify(err, null, 2), ERROR_INDENTATION),
      ].join('\n'),
    );
    console.log('\n');
    log(indent(JSON.stringify(err, null, 2), ERROR_INDENTATION));

    wasSuccessful = false;
  }

  return { ...test, wasSuccessful };
};

const main = async () => {
  const files = await readDir(TESTS_DIR);

  const testFiles = files.filter(f => path.extname(f) === TEST_FILE_EXTENSION);
  const testDescriptors = files.filter(
    f => path.extname(f) === TEST_DESCRIPTOR_EXTENSION,
  );

  info('Checking tests folder...');

  const isValid = checkTests(testFiles, testDescriptors);
  if (!isValid) {
    error(`Tests folder is not valid`);
    process.exit(1);
  }

  success(`Tests folder is valid`);

  const testTuples: [string, string][] = [];
  for (let i = 0; i < testFiles.length; i++) {
    testTuples.push([testFiles[i], testDescriptors[i]]);
  }

  const tests = await createTestInfo(testTuples);
  info('Running tests...');

  const afterRun = await Promise.all(tests.map(async test => runTest(test)));

  const failed = afterRun.filter(t => !t.wasSuccessful).length;
  const successful = afterRun.filter(t => t.wasSuccessful).length;

  info(
    [
      `Successful: ${successful}`,
      indent(`Failed: ${failed}`, INFO_INDENTATION),
    ].join('\n'),
  );
};

main();
