export type AstNodeKind =
  | 'Integer'
  | 'Float'
  | 'Boolean'
  | 'String'
  | 'Array'
  | 'ObjectLiteral'
  | 'Identifier'
  | 'VariableDeclaration'
  | 'Assignment'
  | 'Block'
  | 'Argument'
  | 'FunctionDeclaration'
  | 'FunctionCall'
  | 'BinaryExpression'
  | 'UnaryExpression'
  | 'If'
  | 'While'
  | 'Access'
  | 'Index'
  | 'Import'
  | 'Export'
  | 'ClassDeclaration';

const Integer: AstNodeKind = 'Integer';
const Float: AstNodeKind = 'Float';
const Boolean: AstNodeKind = 'Boolean';
const String: AstNodeKind = 'String';
const Array: AstNodeKind = 'Array';
const ObjectLiteral: AstNodeKind = 'ObjectLiteral';
const Identifier: AstNodeKind = 'Identifier';
const VariableDeclaration: AstNodeKind = 'VariableDeclaration';
const Assignment: AstNodeKind = 'Assignment';
const Block: AstNodeKind = 'Block';
const Argument: AstNodeKind = 'Argument';
const FunctionDeclaration: AstNodeKind = 'FunctionDeclaration';
const FunctionCall: AstNodeKind = 'FunctionCall';
const BinaryExpression: AstNodeKind = 'BinaryExpression';
const UnaryExpression: AstNodeKind = 'UnaryExpression';
const If: AstNodeKind = 'If';
const While: AstNodeKind = 'While';
const Access: AstNodeKind = 'Access';
const Index: AstNodeKind = 'Index';
const Import: AstNodeKind = 'Import';
const Export: AstNodeKind = 'Export';
const ClassDeclaration: AstNodeKind = 'ClassDeclaration';

export const AstNodeKind = {
  Integer,
  Float,
  Boolean,
  String,
  Array,
  ObjectLiteral,
  Identifier,
  VariableDeclaration,
  Assignment,
  Block,
  Argument,
  FunctionDeclaration,
  FunctionCall,
  BinaryExpression,
  UnaryExpression,
  If,
  While,
  Access,
  Index,
  Import,
  Export,
  ClassDeclaration,
};
