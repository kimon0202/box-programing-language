import { AstNodeKind } from './NodeKind';
import {
  Node,
  IntegerLiteralNode,
  FloatLiteralNode,
  BooleanLiteralNode,
  StringLiteralNode,
  ArrayLiteralNode,
  ObjectLiteralNode,
  IdentifierNode,
  VariableDeclarationNode,
  AssignmentNode,
  BlockNode,
  ArgumentNode,
  FunctionDeclarationNode,
  FunctionCallNode,
  BinaryExpressionNode,
  UnaryExpressionNode,
  IfNode,
  WhileNode,
  AccessNode,
  IndexNode,
  ImportNode,
  ExportNode,
  ClassDeclarationNode,
} from './nodes';

const isIntegerLiteralNode = (node: Node): node is IntegerLiteralNode => {
  return node.kind === AstNodeKind.Integer;
};

const isFloatLiteralNode = (node: Node): node is FloatLiteralNode => {
  return node.kind === AstNodeKind.Float;
};

const isBooleanLiteralNode = (node: Node): node is BooleanLiteralNode => {
  return node.kind === AstNodeKind.Boolean;
};

const isStringLiteralNode = (node: Node): node is StringLiteralNode => {
  return node.kind === AstNodeKind.String;
};

const isArrayLiteralNode = (node: Node): node is ArrayLiteralNode => {
  return node.kind === AstNodeKind.Array;
};

const isObjectLiteralNode = (node: Node): node is ObjectLiteralNode => {
  return node.kind === AstNodeKind.ObjectLiteral;
};

const isIdentifierNode = (node: Node): node is IdentifierNode => {
  return node.kind === AstNodeKind.Identifier;
};

const isVariableDeclarationNode = (
  node: Node,
): node is VariableDeclarationNode => {
  return node.kind === AstNodeKind.VariableDeclaration;
};

const isAssignmentNode = (node: Node): node is AssignmentNode => {
  return node.kind === AstNodeKind.Assignment;
};

const isBlockNode = (node: Node): node is BlockNode => {
  return node.kind === AstNodeKind.Block;
};

const isArgumentNode = (node: Node): node is ArgumentNode => {
  return node.kind === AstNodeKind.Argument;
};

const isFunctionDeclarationNode = (
  node: Node,
): node is FunctionDeclarationNode => {
  return node.kind === AstNodeKind.FunctionDeclaration;
};

const isFunctionCallNode = (node: Node): node is FunctionCallNode => {
  return node.kind === AstNodeKind.FunctionCall;
};

const isBinaryExpressionNode = (node: Node): node is BinaryExpressionNode => {
  return node.kind === AstNodeKind.BinaryExpression;
};

const isUnaryExpressionNode = (node: Node): node is UnaryExpressionNode => {
  return node.kind === AstNodeKind.UnaryExpression;
};

const isIfNode = (node: Node): node is IfNode => {
  return node.kind === AstNodeKind.If;
};

const isWhileNode = (node: Node): node is WhileNode => {
  return node.kind === AstNodeKind.While;
};

const isAccessNode = (node: Node): node is AccessNode => {
  return node.kind === AstNodeKind.Access;
};

const isIndexNode = (node: Node): node is IndexNode => {
  return node.kind === AstNodeKind.Index;
};

const isImportNode = (node: Node): node is ImportNode => {
  return node.kind === AstNodeKind.Import;
};

const isExportNode = (node: Node): node is ExportNode => {
  return node.kind === AstNodeKind.Export;
};

const isClassDeclarationNode = (node: Node): node is ClassDeclarationNode => {
  return node.kind === AstNodeKind.ClassDeclaration;
};

export const AstGuards = {
  isIntegerLiteralNode,
  isFloatLiteralNode,
  isBooleanLiteralNode,
  isStringLiteralNode,
  isArrayLiteralNode,
  isObjectLiteralNode,
  isIdentifierNode,
  isVariableDeclarationNode,
  isAssignmentNode,
  isBlockNode,
  isArgumentNode,
  isFunctionDeclarationNode,
  isFunctionCallNode,
  isBinaryExpressionNode,
  isUnaryExpressionNode,
  isIfNode,
  isWhileNode,
  isAccessNode,
  isIndexNode,
  isImportNode,
  isExportNode,
  isClassDeclarationNode,
};
