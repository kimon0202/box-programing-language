/* eslint-disable-next-line max-classes-per-file */
import { AstNodeKind } from './NodeKind';
import { Option } from '../utils/types/Option';
import { Token } from 'src/tokens';
import { Environment } from 'src/runtime/Environment';

export interface IVisitor<Return> {
  visitIntegerLiteralNode(node: IntegerLiteralNode): Return;
  visitFloatLiteralNode(node: FloatLiteralNode): Return;
  visitBooleanLiteralNode(node: BooleanLiteralNode): Return;
  visitStringLiteralNode(node: StringLiteralNode): Return;
  visitArrayLiteralNode(node: ArrayLiteralNode): Return;
  visitObjectLiteralNode(node: ObjectLiteralNode): Return;
  visitIdentifierNode(node: IdentifierNode): Return;
  visitVariableDeclarationNode(node: VariableDeclarationNode): Return;
  visitAssignmentNode(node: AssignmentNode): Return;
  visitBlockNode(node: BlockNode): Return;
  visitArgumentNode(node: ArgumentNode): Return;
  visitFunctionDeclarationNode(node: FunctionDeclarationNode): Return;
  visitFunctionCallNode(node: FunctionCallNode): Return;
  visitBinaryExpressionNode(node: BinaryExpressionNode): Return;
  visitUnaryExpressionNode(node: UnaryExpressionNode): Return;
  visitIfNode(node: IfNode): Return;
  visitWhileNode(node: WhileNode): Return;
  visitAccessNode(node: AccessNode): Return;
  visitIndexNode(node: IndexNode): Return;
  visitImportNode(node: ImportNode): Return;
  visitExportNode(node: ExportNode): Return;
  visitClassDeclarationNode(node: ClassDeclarationNode): Return;
  interpretFunction(node: BlockNode, environment: Environment<Return>): Return;
}

export abstract class Node {
  public readonly kind: AstNodeKind;
  public readonly token: Token;

  public constructor(kind: AstNodeKind, token: Token) {
    this.kind = kind;
    this.token = token;
  }

  public abstract visit<Return>(visitor: IVisitor<Return>): Return;
}

export class IntegerLiteralNode extends Node {
  public constructor(public readonly value: number, token: Token) {
    super(AstNodeKind.Integer, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitIntegerLiteralNode(this);
  }
}

export class FloatLiteralNode extends Node {
  public constructor(public readonly value: number, token: Token) {
    super(AstNodeKind.Float, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitFloatLiteralNode(this);
  }
}

export class BooleanLiteralNode extends Node {
  public constructor(public readonly value: boolean, token: Token) {
    super(AstNodeKind.Boolean, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitBooleanLiteralNode(this);
  }
}

export class StringLiteralNode extends Node {
  public constructor(public readonly value: string, token: Token) {
    super(AstNodeKind.String, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitStringLiteralNode(this);
  }
}

export class ArrayLiteralNode extends Node {
  public constructor(public readonly values: Node[], token: Token) {
    super(AstNodeKind.Array, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitArrayLiteralNode(this);
  }
}

export class ObjectLiteralNode extends Node {
  public constructor(
    public readonly entries: Map<IdentifierNode, Node>,
    token: Token,
  ) {
    super(AstNodeKind.ObjectLiteral, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitObjectLiteralNode(this);
  }
}

export class IdentifierNode extends Node {
  public constructor(public readonly name: string, token: Token) {
    super(AstNodeKind.Identifier, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitIdentifierNode(this);
  }
}

export class VariableDeclarationNode extends Node {
  public constructor(
    public readonly identifier: IdentifierNode,
    public readonly initializer: Node,
    public readonly isMutable: boolean,
    token: Token,
  ) {
    super(AstNodeKind.VariableDeclaration, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitVariableDeclarationNode(this);
  }
}

export class AssignmentNode extends Node {
  public constructor(
    public readonly left: Node,
    public readonly value: Node,
    token: Token,
  ) {
    super(AstNodeKind.Assignment, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitAssignmentNode(this);
  }
}

export class BlockNode extends Node {
  public constructor(public readonly expressions: Node[], token: Token) {
    super(AstNodeKind.Block, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitBlockNode(this);
  }
}

export class ArgumentNode extends Node {
  public constructor(
    public readonly identifier: IdentifierNode,
    public readonly isParams: boolean,
    token: Token,
  ) {
    super(AstNodeKind.Argument, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitArgumentNode(this);
  }
}

export class FunctionDeclarationNode extends Node {
  public constructor(
    public readonly identifier: Option<IdentifierNode>,
    public readonly args: ArgumentNode[],
    public readonly body: BlockNode,
    token: Token,
  ) {
    super(AstNodeKind.FunctionDeclaration, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitFunctionDeclarationNode(this);
  }
}

export class FunctionCallNode extends Node {
  public constructor(
    public readonly left: Node,
    public readonly args: Node[],
    token: Token,
  ) {
    super(AstNodeKind.FunctionCall, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitFunctionCallNode(this);
  }
}

export class BinaryExpressionNode extends Node {
  public constructor(
    public readonly left: Node,
    public readonly operator: IdentifierNode,
    public readonly right: Node,
    token: Token,
  ) {
    super(AstNodeKind.BinaryExpression, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitBinaryExpressionNode(this);
  }
}

export class UnaryExpressionNode extends Node {
  public constructor(
    public readonly operator: IdentifierNode,
    public readonly operand: Node,
    token: Token,
  ) {
    super(AstNodeKind.UnaryExpression, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitUnaryExpressionNode(this);
  }
}

export class IfNode extends Node {
  public constructor(
    public readonly condition: Node,
    public readonly thenBranch: Node,
    public readonly elseBranch: Option<Node>,
    token: Token,
  ) {
    super(AstNodeKind.If, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitIfNode(this);
  }
}

export class WhileNode extends Node {
  public constructor(
    public readonly condition: Node,
    public readonly body: Node,
    token: Token,
  ) {
    super(AstNodeKind.While, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitWhileNode(this);
  }
}

export class AccessNode extends Node {
  public constructor(
    public readonly obj: Node,
    public readonly property: IdentifierNode,
    token: Token,
  ) {
    super(AstNodeKind.Access, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitAccessNode(this);
  }
}

export class IndexNode extends Node {
  public constructor(
    public readonly obj: Node,
    public readonly index: Node,
    token: Token,
  ) {
    super(AstNodeKind.Index, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitIndexNode(this);
  }
}

export class ImportNode extends Node {
  public constructor(
    public readonly path: StringLiteralNode,
    public readonly identifiers: IdentifierNode[],
    token: Token,
  ) {
    super(AstNodeKind.Import, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitImportNode(this);
  }
}

export class ExportNode extends Node {
  public constructor(public readonly item: Node, token: Token) {
    super(AstNodeKind.Export, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitExportNode(this);
  }
}

export class ClassDeclarationNode extends Node {
  public constructor(
    public readonly name: IdentifierNode,
    public readonly superclasses: IdentifierNode[],
    public readonly properties: Map<IdentifierNode, Node>,
    token: Token,
  ) {
    super(AstNodeKind.ClassDeclaration, token);
  }

  public visit<Return>(visitor: IVisitor<Return>): Return {
    return visitor.visitClassDeclarationNode(this);
  }
}
