export class MethodNotImplementedError extends Error {
  public constructor() {
    super(`Error: Method not implemented yet`);
  }
}
