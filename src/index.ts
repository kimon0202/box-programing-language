/* eslint-disable no-await-in-loop */
import { log, logError } from './utils/log';
import { Lexer, SourceCode } from './lexer';
import { BoxParser } from './parser/BoxParser';
import { Interpreter } from './runtime/Interpreter';

type InputCallback = (question: string) => Promise<string>;

const main = async (argv: string[]) => {
  const source = await SourceCode.from(argv[0]);

  const logParserOutput =
    argv.includes('--log-parser-output') || argv.includes('-lp');

  const lexer = new Lexer(source);

  const { tokens, errors } = lexer.tokenize();

  if (errors.length > 0) {
    errors.forEach(logError);
    return;
  }

  // log(tokens);

  const parser = new BoxParser(tokens);
  const { nodes, errors: parserErrors } = parser.parse();

  if (parserErrors.length > 0) {
    parserErrors.forEach(logError);
  }

  if (logParserOutput) {
    log(nodes);
  }

  const interpreter = new Interpreter(source.path);
  const value = interpreter.interpret(nodes);

  log(value);
  log(interpreter.exports);
};

main(process.argv.splice(2));
