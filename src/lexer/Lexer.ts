/* eslint-disable no-continue */
/* eslint-disable no-bitwise */
import { Token, TokenKind } from '../tokens';
import { Char } from '../utils/chars';
import { Location } from '../utils/Location';
import { None, Option, Some } from '../utils/types/Option';
import { findLastIndex } from '../utils/arrays';
import { Result, Ok, Err } from '../utils/types/Result';
import { Keywords } from './keywords';
import { LexerError } from './errors/LexerError';
import { UnterminatedStringError } from './errors/UnterminatedString';
import { SourceCode } from './SourceCode';
import { boundaries } from '../tokens/Boundaries';
import { UnknownEscapeSequenceError } from './errors/UnknownEscapeSequence';

export interface LexerResult {
  readonly tokens: Token[];
  readonly errors: LexerError[];
}

export class Lexer {
  private readonly sourceCode: SourceCode;
  private readonly errors: LexerError[];

  private tokens: Token[] = [];
  private line = 1;
  private column = 1;

  private start = 0;
  private current = 0;

  public constructor(source: SourceCode) {
    this.errors = [];
    this.sourceCode = source;
  }

  private get source(): string {
    return this.sourceCode.content;
  }

  private get isAtEnd(): boolean {
    return this.current > this.sourceCode.content.length;
  }

  private get location(): Location {
    return new Location(
      this.sourceCode.path,
      this.line,
      {
        start: this.column - (this.current - this.start),
        end: this.column,
      },
      {
        start: this.start,
        end: this.current,
      },
    );
  }

  private error(): Token[] {
    this.tokens.push(this.token(TokenKind.Error, ''));

    const startIndex = findLastIndex(this.tokens, token =>
      boundaries.includes(token.kind),
    );
    const tokens = this.tokens.slice(startIndex);

    return tokens;
  }

  private peek(offset = 0): Option<string> {
    const index = this.current + offset;
    return index >= 0 && index < this.source.length
      ? Some(this.source[index])
      : None;
  }

  private advance(): string {
    this.current++;
    this.column++;

    return this.source[this.current - 1];
  }

  private check(expected: string): boolean {
    if (this.isAtEnd) {
      return false;
    }

    return Option.match(this.peek(), {
      Some: char => char === expected,
      None: () => false,
    });
  }

  private matches(expected: string): boolean {
    if (this.isAtEnd) {
      return false;
    }

    return this.check(expected) ? !!this.advance() : false;
  }

  private token(kind: TokenKind, lexeme: string): Token {
    return new Token(kind, lexeme, this.location);
  }

  private handleNumbers() {
    while (Char.isDigit(this.peek()) || Option.is(this.peek(), '_')) {
      this.advance();
    }

    if (Option.is(this.peek(), '.') && Char.isDigit(this.peek(1))) {
      this.advance();

      while (Char.isDigit(this.peek()) || Option.is(this.peek(), '_')) {
        this.advance();
      }
    }

    const lexeme = this.source
      .substring(this.start, this.current)
      .replace(/_/g, '');
    const kind = lexeme.includes('.') ? TokenKind.Float : TokenKind.Integer;

    return this.token(kind, lexeme);
  }

  private handleIdentifiers() {
    while (Char.isAlphaNumeric(this.peek()) || Char.isSymbol(this.peek())) {
      this.advance();
    }

    const lexeme = this.source.substring(this.start, this.current);
    const kind = Keywords.get(lexeme) || TokenKind.Identifier;

    return this.token(kind, lexeme);
  }

  // TODO: add interpolation (based on the implementation in Wren)
  // TODO: add unicode and hex escape sequences
  private handleStrings(): Result<Token, LexerError> {
    const stringBuffer: string[] = [];
    const errorsBuffer: LexerError[] = [];

    for (;;) {
      const c = this.advance();

      if (c === "'") {
        break;
      }

      if (c === '\r') {
        continue;
      }

      // TODO: should newlines terminate strings as errors?
      if (this.isAtEnd || c === '\n') {
        const tokens = this.error();

        return Err(
          new UnterminatedStringError(
            this.location,
            {
              start: this.start,
              end: this.current,
            },
            tokens,
          ),
        );
      }

      if (c === '\\') {
        const next = this.advance();

        switch (next) {
          case '\\':
            stringBuffer.push('\\');
            break;
          case "'":
            stringBuffer.push("'");
            break;
          case '0':
            stringBuffer.push('\0');
            break;
          case 'a':
            stringBuffer.push('a');
            break;
          case 'b':
            stringBuffer.push('\b');
            break;
          case 'f':
            stringBuffer.push('\f');
            break;
          case 'n':
            stringBuffer.push('\n');
            break;
          case 'r':
            stringBuffer.push('\r');
            break;
          case 't':
            stringBuffer.push('\t');
            break;
          // case 'u':
          //   {
          //     const unicode = this.readUnicodeEscape(4);

          //     if (Result.isErr(unicode)) {
          //       return unicode;
          //     }

          //     stringBuffer.push(String(unicode.value));
          //   }
          //   break;
          // case 'U':
          //   {
          //     const unicode = this.readUnicodeEscape(8);

          //     if (Result.isErr(unicode)) {
          //       return unicode;
          //     }

          //     stringBuffer.push(String(unicode));
          //   }
          //   break;
          case 'v':
            stringBuffer.push('\v');
            break;
          // case 'x':
          //   {
          //     const hex = this.readHexEscape(2, 'byte');

          //     if (Result.isErr(hex)) {
          //       return hex;
          //     }

          //     stringBuffer.push(String(hex));
          //   }
          //   break;
          default:
            {
              const tokens = this.error();
              const error = new UnknownEscapeSequenceError(
                `\\${next}`,
                this.location,
                {
                  start: this.start,
                  end: this.current,
                },
                tokens,
              );

              errorsBuffer.push(error);
            }
            break;
        }
      } else {
        stringBuffer.push(c);
      }
    }

    if (errorsBuffer.length > 0) {
      return Err(errorsBuffer[0]);
    }

    return Ok(this.token(TokenKind.String, stringBuffer.join('')));
  }

  private skipWhitespace() {
    if (Char.isWhitespace(this.peek())) {
      while (Char.isWhitespace(this.peek())) {
        if (Option.is(this.peek(), '\n')) {
          this.line++;
          this.column = 0;
        }

        this.advance();
      }
    }
  }

  private skipLineComments() {
    if (Option.is(this.peek(), '/') && Option.is(this.peek(1), '/')) {
      while (!this.isAtEnd && !Option.is(this.peek(), '\n')) {
        this.advance();
      }

      this.advance();
    }

    this.skipWhitespace();
  }

  private next(): Result<Token, LexerError> {
    this.skipWhitespace();
    this.skipLineComments();

    this.start = this.current;
    const char = this.advance();

    if (!char || char === '\0') return Ok(this.token(TokenKind.Eof, ''));
    if (Char.isDigit(char)) return Ok(this.handleNumbers());

    switch (char) {
      case '(':
        return Ok(this.token(TokenKind.OpenParen, char));
      case ')':
        return Ok(this.token(TokenKind.CloseParen, char));
      case '[':
        return Ok(this.token(TokenKind.OpenBracket, char));
      case ']':
        return Ok(this.token(TokenKind.CloseBracket, char));
      case '{':
        return Ok(this.token(TokenKind.OpenCurly, char));
      case '}':
        return Ok(this.token(TokenKind.CloseCurly, char));
      case ':':
        return Ok(
          this.matches(':')
            ? this.token(TokenKind.ColonColon, '::')
            : this.token(TokenKind.Colon, char),
        );
      case '.':
        return Ok(
          this.matches('.')
            ? this.token(TokenKind.DotDot, '..')
            : this.token(TokenKind.Dot, char),
        );
      case ',':
        return Ok(this.token(TokenKind.Comma, char));
      case '+':
        return Ok(this.token(TokenKind.Plus, char));
      case '-':
        return Ok(
          this.matches('>')
            ? this.token(TokenKind.Arrow, '->')
            : this.token(TokenKind.Minus, char),
        );
      case '*':
        return Ok(this.token(TokenKind.Star, char));
      case '/':
        return Ok(this.token(TokenKind.Slash, char));
      case '%':
        return Ok(
          this.matches('{')
            ? this.token(TokenKind.PercentCurly, '%{')
            : this.token(TokenKind.Modulo, char),
        );
      case '<':
        return Ok(
          this.matches('=')
            ? this.token(TokenKind.LessEqual, '<=')
            : this.token(TokenKind.Less, char),
        );
      case '>':
        return Ok(
          this.matches('=')
            ? this.token(TokenKind.GreaterEqual, '>=')
            : this.token(TokenKind.Greater, char),
        );
      case '=':
        return Ok(
          this.matches('=')
            ? this.token(TokenKind.EqualEqual, '==')
            : this.token(TokenKind.Equal, char),
        );
      case '!':
        return Ok(
          this.matches('=')
            ? this.token(TokenKind.BangEqual, '!=')
            : this.token(TokenKind.Bang, char),
        );
      case '|':
        return Ok(this.token(TokenKind.Pipe, char));
      case "'":
        return this.handleStrings();
      default:
        return Ok(this.handleIdentifiers());
    }
  }

  public tokenize(): LexerResult {
    while (!this.isAtEnd) {
      const result = this.next();

      Result.match(result, {
        Ok: token => {
          this.tokens.push(token);
        },
        Error: error => {
          this.errors.push(error);
        },
      });
    }

    // this.tokens.push(this.token(TokenKind.Eof, ''));
    return {
      tokens: this.tokens,
      errors: this.errors,
    };
  }
}
