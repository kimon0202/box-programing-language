import { readFileSync } from 'fs';
import { readFile } from 'fs/promises';

export interface SourceCode {
  readonly content: string;
  readonly path: string;
}

export const SourceCode = {
  from: async (path: string): Promise<SourceCode> => {
    const content = await readFile(path, { encoding: 'utf8' });
    return { content, path };
  },

  fromSync: (path: string): SourceCode => {
    const content = readFileSync(path, { encoding: 'utf8' });
    return { content, path };
  },
};
