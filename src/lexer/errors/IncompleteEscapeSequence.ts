import { Token } from '../../tokens';
import { Location, Span } from '../../utils/Location';
import { LexerError } from './LexerError';

export class IncompleteEscapeSequenceError extends LexerError {
  public constructor(
    public readonly description: string,
    location: Location,
    span: Span,
    tokens: Token[],
  ) {
    super(`Incomplete ${description} escape sequence`, location, span, tokens);
  }
}
