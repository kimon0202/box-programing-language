import { Token } from '../../tokens';
import { Location, Span } from '../../utils/Location';

export class LexerError extends Error {
  public constructor(
    public readonly message: string,
    public readonly location: Location,
    public readonly span: Span,
    public readonly tokens: Token[],
  ) {
    super(`${message} at ${location.toString()}`);
  }
}
