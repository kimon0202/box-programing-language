import { Token } from '../../tokens';
import { Location, Span } from '../../utils/Location';
import { escape } from '../../utils/strings';
import { LexerError } from './LexerError';

export class UnknownEscapeSequenceError extends LexerError {
  public constructor(
    public readonly sequence: string,
    location: Location,
    span: Span,
    tokens: Token[],
  ) {
    super(
      `Unknown escape sequence ${escape(sequence)}`,
      location,
      span,
      tokens,
    );
  }
}
