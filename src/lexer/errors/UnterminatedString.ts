import { Token } from '../../tokens';
import { Location, Span } from '../../utils/Location';
import { LexerError } from './LexerError';

export class UnterminatedStringError extends LexerError {
  public constructor(location: Location, span: Span, tokens: Token[]) {
    super(`Unterminated string`, location, span, tokens);
  }
}
