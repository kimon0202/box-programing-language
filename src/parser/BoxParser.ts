import { Parser } from './Parser';
import { Token, TokenKind } from '../tokens';
import { IntegerLiteralParselet } from './parselets/IntegerLiteralParselet';
import { BooleanLiteralParselet } from './parselets/BooleanLiteralParselet';
import { FloatLiteralParselet } from './parselets/FloatLiteralParselet';
import { StringLiteralParselet } from './parselets/StringLiteralParselet';
import { IdentifierParselet } from './parselets/IdentifierParselet';
import { BinaryParselet } from './parselets/BinaryParselet';
import { Precedence } from './Precedence';
import { GroupParselet } from './parselets/GroupParselet';
import { AssignmentParselet } from './parselets/AssignmentParselet';
import { IfParselet } from './parselets/IfParselet';
import { BlockParselet } from './parselets/BlockParselet';
import { UnaryParselet } from './parselets/UnaryParselet';
import { WhileParselet } from './parselets/WhileParselet';
import { ObjectLiteralParselet } from './parselets/ObjectLiteralParselet';
import { FunctionCallParselet } from './parselets/FunctionCallParselet';
import { ArrayLiteralParselet } from './parselets/ArrayLiteralParselet';
import { PropertyAccessParselet } from './parselets/PropertyAccessParselet';
import { IndexAccessParselet } from './parselets/IndexAccessParselet';
import { ParamsParselet } from './parselets/ParamsParselet';

export class BoxParser extends Parser {
  public constructor(tokens: Token[]) {
    super(tokens);

    this.prefixParselets.set(TokenKind.Integer, new IntegerLiteralParselet());
    this.prefixParselets.set(TokenKind.Float, new FloatLiteralParselet());
    this.prefixParselets.set(TokenKind.String, new StringLiteralParselet());
    this.prefixParselets.set(TokenKind.True, new BooleanLiteralParselet());
    this.prefixParselets.set(TokenKind.False, new BooleanLiteralParselet());
    this.prefixParselets.set(TokenKind.Params, new ParamsParselet());

    this.prefixParselets.set(TokenKind.Identifier, new IdentifierParselet());
    this.prefixParselets.set(TokenKind.OpenParen, new GroupParselet());
    this.prefixParselets.set(TokenKind.OpenCurly, new BlockParselet());
    this.prefixParselets.set(TokenKind.OpenBracket, new ArrayLiteralParselet());

    this.prefixParselets.set(TokenKind.If, new IfParselet());
    this.prefixParselets.set(TokenKind.While, new WhileParselet());

    this.prefixParselets.set(TokenKind.Minus, new UnaryParselet());
    this.prefixParselets.set(TokenKind.Bang, new UnaryParselet());
    this.prefixParselets.set(TokenKind.Not, new UnaryParselet());

    this.infixParselets.set(TokenKind.OpenParen, new FunctionCallParselet());
    this.infixParselets.set(TokenKind.Dot, new PropertyAccessParselet());
    this.infixParselets.set(TokenKind.OpenBracket, new IndexAccessParselet());

    this.prefixParselets.set(
      TokenKind.PercentCurly,
      new ObjectLiteralParselet(),
    );

    this.infixParselets.set(
      TokenKind.Plus,
      new BinaryParselet(Precedence.Term),
    );

    this.infixParselets.set(
      TokenKind.Minus,
      new BinaryParselet(Precedence.Term),
    );

    this.infixParselets.set(
      TokenKind.Star,
      new BinaryParselet(Precedence.Factor),
    );

    this.infixParselets.set(
      TokenKind.Slash,
      new BinaryParselet(Precedence.Factor),
    );

    this.infixParselets.set(
      TokenKind.Modulo,
      new BinaryParselet(Precedence.Factor),
    );

    this.infixParselets.set(
      TokenKind.EqualEqual,
      new BinaryParselet(Precedence.Equality),
    );

    this.infixParselets.set(
      TokenKind.BangEqual,
      new BinaryParselet(Precedence.Equality),
    );

    this.infixParselets.set(
      TokenKind.Greater,
      new BinaryParselet(Precedence.Comparison),
    );

    this.infixParselets.set(
      TokenKind.GreaterEqual,
      new BinaryParselet(Precedence.Comparison),
    );

    this.infixParselets.set(
      TokenKind.Less,
      new BinaryParselet(Precedence.Comparison),
    );

    this.infixParselets.set(
      TokenKind.LessEqual,
      new BinaryParselet(Precedence.Comparison),
    );

    this.infixParselets.set(
      TokenKind.And,
      new BinaryParselet(Precedence.LogicalAnd),
    );

    this.infixParselets.set(
      TokenKind.Or,
      new BinaryParselet(Precedence.LogicalOr),
    );

    this.infixParselets.set(TokenKind.Equal, new AssignmentParselet());
  }
}
