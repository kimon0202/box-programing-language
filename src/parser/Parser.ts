/* eslint-disable no-continue */
import { AstGuards } from 'src/ast';

import { Token } from '../tokens/Token';
import { Precedence } from './Precedence';
import {
  Node,
  VariableDeclarationNode,
  IdentifierNode,
  BlockNode,
  FunctionDeclarationNode,
  ExportNode,
  ImportNode,
  ClassDeclarationNode,
  ArgumentNode,
} from '../ast/nodes';
import { TokenKind } from '../tokens/TokenKind';
import { Option, None, Some } from '../utils/types/Option';
import { Result, Err, Ok } from '../utils/types/Result';
import { ParserError } from './errors/ParserError';
import { MissingPrefixParseletError } from './errors/MissingPrefixParselet';
import { MissingInfixParseletError } from './errors/MissingInfixParselet';
import { ExpectedIdentifierError } from './errors/ExpectedIdentifier';
import { ExpectedEqualsError } from './errors/ExpectedEquals';
import { ExpectedBlockClosingError } from './errors/ExpectedBlockClosing';
import { ExpectedOpeningParenthesisError } from './errors/ExpectedOpeningParenthesis';
import { ExpectedCloseParenthesisError } from './errors/ExpectedCloseParenthesis';
import { ExpectedBlockError } from './errors/ExpectedBlock';
import { ExpectedStringError } from './errors/ExpectedString';
import { ExpectedImportError } from './errors/ExpectedImport';
import { ExpectedFunctionToHaveNameError } from './errors/ExpectedFunctionToHaveName';

export interface IPrefixParselet {
  parse(parser: Parser, token: Token): Result<Node, ParserError>;
}

export interface IInfixParselet {
  readonly precedence: Precedence;
  parse(parser: Parser, left: Node, token: Token): Result<Node, ParserError>;
}

export interface ParserResult {
  readonly nodes: Node[];
  readonly errors: ParserError[];
}

export class Parser {
  protected readonly tokens: Token[];
  protected readonly errors: ParserError[];

  protected readonly infixParselets: Map<TokenKind, IInfixParselet>;
  protected readonly prefixParselets: Map<TokenKind, IPrefixParselet>;

  protected start = 0;
  protected current = 0;

  public constructor(tokens: Token[]) {
    this.tokens = tokens;
    this.errors = [];

    this.infixParselets = new Map<TokenKind, IInfixParselet>();
    this.prefixParselets = new Map<TokenKind, IPrefixParselet>();
  }

  protected get isAtEnd(): boolean {
    return (
      this.current >= this.tokens.length ||
      this.tokens[this.current].is(TokenKind.Eof)
    );
  }

  protected get currentPrecedence(): Precedence {
    return Option.match(this.peek(), {
      Some: token =>
        this.infixParselets.get(token.kind)?.precedence || Precedence.None,
      None: () => Precedence.None,
    });
  }

  public get previous(): Token {
    return this.tokens[this.current - 1];
  }

  protected advance(): Token {
    this.current++;
    return this.tokens[this.current - 1];
  }

  public peek(offset = 0): Option<Token> {
    const index = this.current + offset;

    if (index >= this.tokens.length) {
      return None;
    }

    return Some(this.tokens[index]);
  }

  public matches(kind: TokenKind): boolean {
    return Option.match(this.peek(), {
      Some: token => {
        if (!token.is(kind)) {
          return false;
        }

        this.advance();
        return true;
      },
      None: () => false,
    });
  }

  public expect(kind: TokenKind): { matches: boolean; token: Token } {
    const token = this.advance();

    if (!token.is(kind)) {
      return { matches: false, token };
    }

    return { matches: true, token };
  }

  public parseExpression(
    precedence = Precedence.None,
  ): Result<Node, ParserError> {
    const token = this.advance();
    const prefixParselet = this.prefixParselets.get(token.kind);

    if (!prefixParselet) {
      return Err(new MissingPrefixParseletError(token));
    }

    let result = prefixParselet.parse(this, token);
    while (precedence < this.currentPrecedence) {
      if (Result.isErr(result)) {
        return result;
      }

      const innerToken = this.advance();
      const infixParselet = this.infixParselets.get(innerToken.kind);

      if (!infixParselet) {
        return Err(new MissingInfixParseletError(innerToken));
      }

      result = infixParselet.parse(this, result.value, innerToken);

      if (Result.isErr(result)) {
        return result;
      }
    }

    return result;
  }

  public parseBlock(): Result<Node, ParserError> {
    const expressions: Node[] = [];

    const startingToken = this.previous;
    if (!this.matches(TokenKind.CloseCurly)) {
      while (!Option.is(this.peek(), token => token.is(TokenKind.CloseCurly))) {
        const expression = this.parseDeclarationOrExpression();

        if (Result.isErr(expression)) {
          return expression;
        }

        expressions.push(expression.value);
      }

      const result = this.expect(TokenKind.CloseCurly);

      if (!result.matches) {
        return Err(new ExpectedBlockClosingError(result.token));
      }
    }

    return Ok(new BlockNode(expressions, startingToken));
  }

  private parseVariableDeclaration(): Result<Node, ParserError> {
    const startingToken = this.previous;
    const expectNameTokenResult = this.expect(TokenKind.Identifier);

    if (!expectNameTokenResult.matches) {
      return Err(new ExpectedIdentifierError(expectNameTokenResult.token));
    }

    const name = expectNameTokenResult.token.lexeme;

    const expectEqualsTokenResult = this.expect(TokenKind.Equal);

    if (!expectEqualsTokenResult.matches) {
      return Err(new ExpectedEqualsError(expectEqualsTokenResult.token));
    }

    const value = this.parseExpressionOrFunctionDeclaration();

    if (Result.isErr(value)) {
      return value;
    }

    const identifier = new IdentifierNode(name, expectNameTokenResult.token);
    return Ok(
      new VariableDeclarationNode(identifier, value.value, true, startingToken),
    );
  }

  private parseFunctionDeclaration(): Result<
    FunctionDeclarationNode,
    ParserError
  > {
    const startingToken = this.previous;
    const nameToken = Option.match(this.peek(), {
      Some: tok => (tok.is(TokenKind.Identifier) ? Some(this.advance()) : None),
      None: () => None,
    });

    const expectOpeningParen = this.expect(TokenKind.OpenParen);

    if (!expectOpeningParen.matches) {
      return Err(new ExpectedOpeningParenthesisError(expectOpeningParen.token));
    }

    const args: ArgumentNode[] = [];

    if (!this.matches(TokenKind.CloseParen)) {
      while (!Option.is(this.peek(), token => token.is(TokenKind.CloseParen))) {
        const arg = this.parseExpression();

        if (Result.isErr(arg)) {
          return arg;
        }

        if (
          !AstGuards.isIdentifierNode(arg.value) &&
          !AstGuards.isArgumentNode(arg.value)
        ) {
          return Err(new ExpectedIdentifierError(arg.value));
        }

        const argNode = AstGuards.isIdentifierNode(arg.value)
          ? new ArgumentNode(arg.value, false, arg.value.token)
          : arg.value;

        args.push(argNode);
        this.matches(TokenKind.Comma);
      }

      const expectClosingParen = this.expect(TokenKind.CloseParen);

      if (!expectClosingParen.matches) {
        return Err(new ExpectedCloseParenthesisError(expectClosingParen.token));
      }
    }

    const expectBlockOpeningResult = this.expect(TokenKind.OpenCurly);

    if (!expectBlockOpeningResult.matches) {
      return Err(new ExpectedBlockError(expectBlockOpeningResult.token));
    }

    const body = this.parseBlock();

    if (Result.isErr(body)) {
      return body;
    }

    const identifier = Option.match(nameToken, {
      Some: token => Some(new IdentifierNode(token.lexeme, token)),
      None: () => None,
    });
    return Ok(
      new FunctionDeclarationNode(
        identifier,
        args,
        body.value as BlockNode,
        startingToken,
      ),
    );
  }

  private parseExportDeclaration(): Result<Node, ParserError> {
    const startingToken = this.previous;
    const expression = this.parseDeclarationOrExpression();

    if (Result.isErr(expression)) {
      return expression;
    }

    return Ok(new ExportNode(expression.value, startingToken));
  }

  private parseImportDeclaration(): Result<Node, ParserError> {
    const startingToken = this.previous;
    const path = this.parseExpression();

    if (Result.isErr(path)) {
      return path;
    }

    if (!AstGuards.isStringLiteralNode(path.value)) {
      return Err(new ExpectedStringError(path.value));
    }

    const expectImportToken = this.expect(TokenKind.Import);

    if (!expectImportToken.matches) {
      return Err(new ExpectedImportError(expectImportToken.token));
    }

    const expectBlockOpeningResult = this.expect(TokenKind.OpenCurly);

    if (!expectBlockOpeningResult.matches) {
      return Err(new ExpectedBlockError(expectBlockOpeningResult.token));
    }

    const imports: IdentifierNode[] = [];
    if (!this.matches(TokenKind.CloseCurly)) {
      while (!Option.is(this.peek(), token => token.is(TokenKind.CloseCurly))) {
        const importIdentifier = this.parseExpression();

        if (Result.isErr(importIdentifier)) {
          return importIdentifier;
        }

        if (!AstGuards.isIdentifierNode(importIdentifier.value)) {
          return Err(new ExpectedIdentifierError(importIdentifier.value));
        }

        imports.push(importIdentifier.value);
        this.matches(TokenKind.Comma);
      }

      const expectClosingCurly = this.expect(TokenKind.CloseCurly);

      if (!expectClosingCurly.matches) {
        return Err(new ExpectedBlockClosingError(expectClosingCurly.token));
      }
    }

    return Ok(new ImportNode(path.value, imports, startingToken));
  }

  // TODO: add superclasses parsing
  private parseClassDeclaration(): Result<Node, ParserError> {
    const startingToken = this.previous;
    const expectNameToken = this.expect(TokenKind.Identifier);

    if (!expectNameToken.matches) {
      return Err(new ExpectedIdentifierError(expectNameToken.token));
    }

    const expectBlockOpeningResult = this.expect(TokenKind.OpenCurly);

    if (!expectBlockOpeningResult.matches) {
      return Err(new ExpectedBlockError(expectBlockOpeningResult.token));
    }

    const properties: Map<IdentifierNode, Node> = new Map();
    if (!this.matches(TokenKind.CloseCurly)) {
      while (!Option.is(this.peek(), token => token.is(TokenKind.CloseCurly))) {
        const property = this.parseFunctionDeclaration();

        if (Result.isErr(property)) {
          return property;
        }

        if (Option.isNone(property.value.identifier)) {
          return Err(new ExpectedFunctionToHaveNameError(property.value.token));
        }

        properties.set(property.value.identifier.value, property.value);
      }

      const expectClosingCurly = this.expect(TokenKind.CloseCurly);

      if (!expectClosingCurly.matches) {
        return Err(new ExpectedBlockClosingError(expectClosingCurly.token));
      }
    }

    const identifier = new IdentifierNode(
      expectNameToken.token.lexeme,
      expectNameToken.token,
    );

    return Ok(
      new ClassDeclarationNode(identifier, [], properties, startingToken),
    );
  }

  public parseExpressionOrFunctionDeclaration(): Result<Node, ParserError> {
    if (this.matches(TokenKind.Fun)) {
      return this.parseFunctionDeclaration();
    }

    return this.parseExpression();
  }

  public parseDeclarationOrExpression(): Result<Node, ParserError> {
    if (this.matches(TokenKind.Let)) {
      return this.parseVariableDeclaration();
    }

    if (this.matches(TokenKind.Fun)) {
      return this.parseFunctionDeclaration();
    }

    if (this.matches(TokenKind.Export)) {
      return this.parseExportDeclaration();
    }

    if (this.matches(TokenKind.From)) {
      return this.parseImportDeclaration();
    }

    if (this.matches(TokenKind.Class)) {
      return this.parseClassDeclaration();
    }

    return this.parseExpression();
  }

  public parse(): ParserResult {
    const nodes: Node[] = [];

    while (!this.isAtEnd) {
      const node = this.parseDeclarationOrExpression();

      if (Result.isErr(node)) {
        this.errors.push(node.value);
        continue;
      }

      nodes.push(node.value);
    }

    return { nodes, errors: this.errors };
  }
}
