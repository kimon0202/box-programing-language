export const Precedence = {
  None: 0,
  Assignment: 1,
  Conditional: 2,
  Bitwise: 3,
  LogicalOr: 4,
  LogicalAnd: 5,
  Equality: 6,
  Comparison: 7,
  Is: 8,
  In: 9,
  Range: 10,
  Term: 11,
  Factor: 12,
  Exponent: 13,
  Prefix: 14,
  Postfix: 15,
  Call: 16,
  Last: 17,
};

export type Precedence = number;
