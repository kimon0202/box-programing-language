import { Node } from 'src/ast';
import { Token } from 'src/tokens';

import { ParserError } from './ParserError';

export class CannotAssignToError extends ParserError {
  public constructor(public readonly got: Node, token: Token) {
    super(`Cannot assign value to ${got.kind}`, token);
  }
}
