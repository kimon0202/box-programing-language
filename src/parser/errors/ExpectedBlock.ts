import { Token } from 'src/tokens';
import { escape } from 'src/utils/strings';

import { ParserError } from './ParserError';

export class ExpectedBlockError extends ParserError {
  public constructor(token: Token) {
    super(
      `Expected block opening ${escape('{')}, but instead got ${token.lexeme}`,
      token,
    );
  }
}
