import { Token } from 'src/tokens';
import { escape } from 'src/utils/strings';

import { ParserError } from './ParserError';

export class ExpectedBlockClosingError extends ParserError {
  public constructor(token: Token) {
    super(
      `Expected block closing ${escape('}')}, but instead got ${token.lexeme}`,
      token,
    );
  }
}
