import { escape } from 'src/utils/strings';

import { ParserError } from './ParserError';
import { Token } from '../../tokens/Token';

export class ExpectedCloseParenthesisError extends ParserError {
  public constructor(got: Token) {
    super(
      `Expected right parenthesis, but instead got ${escape(got.kind)}`,
      got,
    );
  }
}
