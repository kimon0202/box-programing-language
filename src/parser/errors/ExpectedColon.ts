import { escape } from 'src/utils/strings';

import { ParserError } from './ParserError';
import { Token } from '../../tokens/Token';

export class ExpectedColonError extends ParserError {
  public constructor(got: Token) {
    super(`Expected ${escape(':')}, but instead got ${escape(got.kind)}`, got);
  }
}
