import { escape } from 'src/utils/strings';

import { ParserError } from './ParserError';
import { Token } from '../../tokens/Token';

export class ExpectedEqualsError extends ParserError {
  public constructor(got: Token) {
    super(`Expected equals, but instead got ${escape(got.kind)}`, got);
  }
}
