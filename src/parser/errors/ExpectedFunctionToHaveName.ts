import { Token } from 'src/tokens';

import { ParserError } from './ParserError';

export class ExpectedFunctionToHaveNameError extends ParserError {
  public constructor(token: Token) {
    super('Expected function to have a name', token);
  }
}
