import { escape } from 'src/utils/strings';
import { Node } from 'src/ast';

import { ParserError } from './ParserError';
import { Token } from '../../tokens/Token';

export class ExpectedIdentifierError extends ParserError {
  public constructor(got: Token | Node) {
    super(
      `Expected identifier, but instead got ${escape(got.kind)}`,
      got instanceof Node ? got.token : got,
    );
  }
}
