import { escape } from 'src/utils/strings';

import { ParserError } from './ParserError';
import { Token } from '../../tokens/Token';

export class ExpectedImportError extends ParserError {
  public constructor(got: Token) {
    super(
      `Expected ${escape('import')} keyword, but instead got ${got.lexeme}`,
      got,
    );
  }
}
