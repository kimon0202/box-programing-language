import { Token } from 'src/tokens';

import { ParserError } from './ParserError';

export class ExpectedIndexExpressionError extends ParserError {
  public constructor(token: Token) {
    super(`Expected index expression, but got ${token.kind}`, token);
  }
}
