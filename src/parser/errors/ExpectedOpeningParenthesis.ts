import { escape } from 'src/utils/strings';

import { ParserError } from './ParserError';
import { Token } from '../../tokens/Token';

export class ExpectedOpeningParenthesisError extends ParserError {
  public constructor(got: Token) {
    super(
      `Expected left parenthesis, but instead got ${escape(got.kind)}`,
      got,
    );
  }
}
