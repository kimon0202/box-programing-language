import { Node } from 'src/ast';

import { ParserError } from './ParserError';

export class ExpectedStringError extends ParserError {
  public constructor(public readonly node: Node) {
    super(
      `Expected import path to be a string, but instead got ${node.kind}`,
      node.token,
    );
  }
}
