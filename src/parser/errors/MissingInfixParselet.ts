import { ParserError } from './ParserError';
import { Token } from '../../tokens/Token';
import { escape } from '../../utils/strings';

export class MissingInfixParseletError extends ParserError {
  public constructor(token: Token) {
    super(`Missing infix parselet for ${escape(token.kind)}`, token);
  }
}
