import { ParserError } from './ParserError';
import { Token } from '../../tokens/Token';
import { escape } from '../../utils/strings';

export class MissingPrefixParseletError extends ParserError {
  public constructor(token: Token) {
    super(`Missing prefix parselet for ${escape(token.kind)}`, token);
  }
}
