import { Token } from 'src/tokens';

export abstract class ParserError extends Error {
  public constructor(message: string, public readonly token: Token) {
    super(`${message} at ${token.location.toString()}`);
  }
}
