import { ArrayLiteralNode, Node } from 'src/ast';
import { Token, TokenKind } from 'src/tokens';
import { Option } from 'src/utils/types/Option';
import { Result, Err, Ok } from 'src/utils/types/Result';

import { ParserError } from '../errors/ParserError';
import { IPrefixParselet, Parser } from '../Parser';
import { ExpectedClosingBracketError } from '../errors/ExpectedClosingBracket';

export class ArrayLiteralParselet implements IPrefixParselet {
  public parse(parser: Parser, token: Token): Result<Node, ParserError> {
    const elements: Node[] = [];

    if (!parser.matches(TokenKind.CloseBracket)) {
      while (!Option.is(parser.peek(), tok => tok.is(TokenKind.CloseBracket))) {
        const element = parser.parseExpression();

        if (Result.isErr(element)) {
          return element;
        }

        elements.push(element.value);
        parser.matches(TokenKind.Comma);
      }

      const expectClosingBracketResult = parser.expect(TokenKind.CloseBracket);

      if (!expectClosingBracketResult.matches) {
        return Err(
          new ExpectedClosingBracketError(expectClosingBracketResult.token),
        );
      }
    }

    return Ok(new ArrayLiteralNode(elements, token));
  }
}
