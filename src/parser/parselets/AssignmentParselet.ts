import { AstGuards, Node } from 'src/ast';
import { Err, Result, Ok } from 'src/utils/types/Result';

import { ParserError } from '../errors/ParserError';
import { IInfixParselet, Parser } from '../Parser';
import { Precedence } from '../Precedence';
import { CannotAssignToError } from '../errors/CannotAssignTo';
import { AssignmentNode } from '../../ast/nodes';

export class AssignmentParselet implements IInfixParselet {
  public readonly precedence = Precedence.Assignment;

  public parse(parser: Parser, left: Node): Result<Node, ParserError> {
    if (!AstGuards.isIdentifierNode(left) && !AstGuards.isAccessNode(left)) {
      return Err(new CannotAssignToError(left, left.token));
    }

    const right = parser.parseExpression(this.precedence);

    if (Result.isErr(right)) {
      return right;
    }

    return Ok(new AssignmentNode(left, right.value, left.token));
  }
}
