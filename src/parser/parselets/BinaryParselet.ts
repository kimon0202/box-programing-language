import { Node } from 'src/ast';
import { Token } from 'src/tokens';
import { Result } from 'src/utils/types/Result';

import { ParserError } from '../errors/ParserError';
import { IInfixParselet, Parser } from '../Parser';
import { BinaryExpressionNode, IdentifierNode } from '../../ast/nodes';

export class BinaryParselet implements IInfixParselet {
  public constructor(
    public readonly precedence: number,
    public readonly isRightAssociative = false,
  ) {}

  public parse(
    parser: Parser,
    left: Node,
    token: Token,
  ): Result<Node, ParserError> {
    const precedence = this.precedence - (this.isRightAssociative ? 1 : 0);
    const right = parser.parseExpression(precedence);

    if (Result.isErr(right)) {
      return right;
    }

    const operator = new IdentifierNode(token.lexeme, token);
    return Result.ok(
      new BinaryExpressionNode(left, operator, right.value, left.token),
    );
  }
}
