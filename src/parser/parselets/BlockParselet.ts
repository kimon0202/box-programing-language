import { Node } from 'src/ast';
import { Token } from 'src/tokens';
import { Result } from 'src/utils/types/Result';

import { ParserError } from '../errors/ParserError';
import { IPrefixParselet, Parser } from '../Parser';

export class BlockParselet implements IPrefixParselet {
  public parse(parser: Parser, token: Token): Result<Node, ParserError> {
    return parser.parseBlock();
  }
}
