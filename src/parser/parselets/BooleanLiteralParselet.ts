import { Node, BooleanLiteralNode } from '../../ast/nodes';
import { Token, TokenKind } from '../../tokens';
import { Result, Ok } from '../../utils/types/Result';
import { ParserError } from '../errors/ParserError';
import { IPrefixParselet, Parser } from '../Parser';

export class BooleanLiteralParselet implements IPrefixParselet {
  public parse(parser: Parser, token: Token): Result<Node, ParserError> {
    const value = token.is(TokenKind.True);
    return Ok(new BooleanLiteralNode(value, token));
  }
}
