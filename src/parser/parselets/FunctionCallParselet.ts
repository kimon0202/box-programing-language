import { AstGuards, FunctionCallNode, Node } from 'src/ast';
import { Token } from 'src/tokens';
import { Result, Err, Ok } from 'src/utils/types/Result';
import { Option } from 'src/utils/types/Option';

import { ParserError } from '../errors/ParserError';
import { IInfixParselet, Parser } from '../Parser';
import { Precedence } from '../Precedence';
import { ExpectedIdentifierError } from '../errors/ExpectedIdentifier';
import { TokenKind } from '../../tokens/TokenKind';
import { ExpectedCloseParenthesisError } from '../errors/ExpectedCloseParenthesis';

export class FunctionCallParselet implements IInfixParselet {
  public readonly precedence = Precedence.Call;

  public parse(
    parser: Parser,
    left: Node,
    token: Token,
  ): Result<Node, ParserError> {
    if (!AstGuards.isIdentifierNode(left) && !AstGuards.isAccessNode(left)) {
      return Err(new ExpectedIdentifierError(left));
    }

    const args: Node[] = [];
    if (!parser.matches(TokenKind.CloseParen)) {
      while (!Option.is(parser.peek(), tok => tok.is(TokenKind.CloseParen))) {
        const argument = parser.parseExpressionOrFunctionDeclaration();

        if (Result.isErr(argument)) {
          return argument;
        }

        args.push(argument.value);

        parser.matches(TokenKind.Comma);
      }

      const expectClosingParen = parser.expect(TokenKind.CloseParen);

      if (!expectClosingParen.matches) {
        return Err(new ExpectedCloseParenthesisError(expectClosingParen.token));
      }
    }

    return Ok(new FunctionCallNode(left, args, token));
  }
}
