import { Node } from 'src/ast';
import { Result, Err, Ok } from 'src/utils/types/Result';

import { ParserError } from '../errors/ParserError';
import { IPrefixParselet, Parser } from '../Parser';
import { TokenKind } from '../../tokens/TokenKind';
import { ExpectedCloseParenthesisError } from '../errors/ExpectedCloseParenthesis';

export class GroupParselet implements IPrefixParselet {
  public parse(parser: Parser): Result<Node, ParserError> {
    const value = parser.parseExpression();

    if (Result.isErr(value)) {
      return value;
    }

    const result = parser.expect(TokenKind.CloseParen);
    if (!result.matches) {
      return Err(new ExpectedCloseParenthesisError(result.token));
    }

    return Ok(value.value);
  }
}
