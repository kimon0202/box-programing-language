import { Node } from 'src/ast';
import { Token } from 'src/tokens';
import { Result, Ok } from 'src/utils/types/Result';

import { ParserError } from '../errors/ParserError';
import { IPrefixParselet, Parser } from '../Parser';
import { IdentifierNode } from '../../ast/nodes';

export class IdentifierParselet implements IPrefixParselet {
  public parse(parser: Parser, token: Token): Result<Node, ParserError> {
    return Ok(new IdentifierNode(token.lexeme, token));
  }
}
