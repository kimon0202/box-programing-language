import { IfNode, Node } from 'src/ast';
import { Token, TokenKind } from 'src/tokens';
import { Option } from 'src/utils/types/Option';
import { Err, Ok, Result } from 'src/utils/types/Result';

import { ExpectedBlockError } from '../errors/ExpectedBlock';
import { ParserError } from '../errors/ParserError';
import { IPrefixParselet, Parser } from '../Parser';

export class IfParselet implements IPrefixParselet {
  public parse(parser: Parser, token: Token): Result<Node, ParserError> {
    const condition = parser.parseExpression();

    if (Result.isErr(condition)) {
      return condition;
    }

    const expectBlockOpeningResult = parser.expect(TokenKind.OpenCurly);

    if (!expectBlockOpeningResult.matches) {
      return Err(new ExpectedBlockError(expectBlockOpeningResult.token));
    }

    const thenBranch = parser.parseBlock();

    if (Result.isErr(thenBranch)) {
      return thenBranch;
    }

    let elseBranch: Option<Node> = Option.none();
    if (parser.matches(TokenKind.Else)) {
      const expression = parser.parseExpression();

      if (Result.isErr(expression)) {
        return expression;
      }

      elseBranch = Option.some(expression.value);
    }

    return Ok(new IfNode(condition.value, thenBranch.value, elseBranch, token));
  }
}
