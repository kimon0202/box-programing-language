import { IndexNode, Node } from 'src/ast';
import { Token, TokenKind } from 'src/tokens';
import { Result, Err, Ok } from 'src/utils/types/Result';

import { ParserError } from '../errors/ParserError';
import { IInfixParselet, Parser } from '../Parser';
import { Precedence } from '../Precedence';
import { ExpectedIndexExpressionError } from '../errors/ExpectedIndexExpression';
import { ExpectedClosingBracketError } from '../errors/ExpectedClosingBracket';

export class IndexAccessParselet implements IInfixParselet {
  public readonly precedence = Precedence.Call;

  public parse(
    parser: Parser,
    left: Node,
    token: Token,
  ): Result<Node, ParserError> {
    if (parser.matches(TokenKind.CloseBracket)) {
      return Err(new ExpectedIndexExpressionError(parser.previous));
    }

    const index = parser.parseExpression(this.precedence);

    if (Result.isErr(index)) {
      return index;
    }

    const expectedClosingBracketResult = parser.expect(TokenKind.CloseBracket);

    if (!expectedClosingBracketResult.matches) {
      return Err(
        new ExpectedClosingBracketError(expectedClosingBracketResult.token),
      );
    }

    return Ok(new IndexNode(left, index.value, token));
  }
}
