import { Node, IntegerLiteralNode } from '../../ast/nodes';
import { Token } from '../../tokens';
import { Result, Ok } from '../../utils/types/Result';
import { ParserError } from '../errors/ParserError';
import { IPrefixParselet, Parser } from '../Parser';

export class IntegerLiteralParselet implements IPrefixParselet {
  public parse(parser: Parser, token: Token): Result<Node, ParserError> {
    const number = Number(token.lexeme);
    return Ok(new IntegerLiteralNode(number, token));
  }
}
