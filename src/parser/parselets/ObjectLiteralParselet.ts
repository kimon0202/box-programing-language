import { IdentifierNode, Node, ObjectLiteralNode } from 'src/ast';
import { Token } from 'src/tokens';
import { Result, Err, Ok } from 'src/utils/types/Result';
import { Option } from 'src/utils/types/Option';

import { ParserError } from '../errors/ParserError';
import { IPrefixParselet, Parser } from '../Parser';
import { TokenKind } from '../../tokens/TokenKind';
import { ExpectedIdentifierError } from '../errors/ExpectedIdentifier';
import { ExpectedColonError } from '../errors/ExpectedColon';
import { ExpectedBlockClosingError } from '../errors/ExpectedBlockClosing';

export class ObjectLiteralParselet implements IPrefixParselet {
  public parse(parser: Parser, token: Token): Result<Node, ParserError> {
    const entries: Map<IdentifierNode, Node> = new Map();

    if (!parser.matches(TokenKind.CloseCurly)) {
      while (!Option.is(parser.peek(), tok => tok.is(TokenKind.CloseCurly))) {
        const expectKeyTokenResult = parser.expect(TokenKind.Identifier);

        if (!expectKeyTokenResult.matches) {
          return Err(new ExpectedIdentifierError(expectKeyTokenResult.token));
        }

        const expectedColonToken = parser.expect(TokenKind.Colon);

        if (!expectedColonToken.matches) {
          return Err(new ExpectedColonError(expectedColonToken.token));
        }

        const value = parser.parseExpression();

        if (Result.isErr(value)) {
          return value;
        }

        const identifier = new IdentifierNode(
          expectKeyTokenResult.token.lexeme,
          expectKeyTokenResult.token,
        );
        entries.set(identifier, value.value);

        parser.matches(TokenKind.Comma);
      }

      const expectClosingCurlyToken = parser.expect(TokenKind.CloseCurly);

      if (!expectClosingCurlyToken.matches) {
        return Err(
          new ExpectedBlockClosingError(expectClosingCurlyToken.token),
        );
      }
    }

    return Ok(new ObjectLiteralNode(entries, token));
  }
}
