import { ArgumentNode, IdentifierNode, Node } from 'src/ast';
import { Token } from 'src/tokens';
import { Err, Result, Ok } from 'src/utils/types/Result';

import { ParserError } from '../errors/ParserError';
import { IPrefixParselet, Parser } from '../Parser';
import { TokenKind } from '../../tokens/TokenKind';
import { ExpectedIdentifierError } from '../errors/ExpectedIdentifier';

export class ParamsParselet implements IPrefixParselet {
  public parse(parser: Parser, token: Token): Result<Node, ParserError> {
    const expectIdentifierToken = parser.expect(TokenKind.Identifier);

    if (!expectIdentifierToken.matches) {
      return Err(new ExpectedIdentifierError(expectIdentifierToken.token));
    }

    const identifier = new IdentifierNode(
      expectIdentifierToken.token.lexeme,
      expectIdentifierToken.token,
    );

    return Ok(new ArgumentNode(identifier, true, token));
  }
}
