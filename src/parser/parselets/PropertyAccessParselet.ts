import { AstGuards, Node } from 'src/ast';
import { Token } from 'src/tokens';
import { Result, Err, Ok } from 'src/utils/types/Result';

import { ParserError } from '../errors/ParserError';
import { IInfixParselet, Parser } from '../Parser';
import { Precedence } from '../Precedence';
import { ExpectedIdentifierError } from '../errors/ExpectedIdentifier';
import { AccessNode } from '../../ast/nodes';

export class PropertyAccessParselet implements IInfixParselet {
  public readonly precedence = Precedence.Call;

  public parse(
    parser: Parser,
    left: Node,
    token: Token,
  ): Result<Node, ParserError> {
    const right = parser.parseExpression(this.precedence);

    if (Result.isErr(right)) {
      return right;
    }

    if (!AstGuards.isIdentifierNode(right.value)) {
      return Err(new ExpectedIdentifierError(right.value));
    }

    return Ok(new AccessNode(left, right.value, token));
  }
}
