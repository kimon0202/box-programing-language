import { Node, StringLiteralNode } from '../../ast/nodes';
import { Token } from '../../tokens';
import { Result, Ok } from '../../utils/types/Result';
import { ParserError } from '../errors/ParserError';
import { IPrefixParselet, Parser } from '../Parser';

export class StringLiteralParselet implements IPrefixParselet {
  public parse(parser: Parser, token: Token): Result<Node, ParserError> {
    return Ok(new StringLiteralNode(token.lexeme, token));
  }
}
