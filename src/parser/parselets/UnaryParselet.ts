import { IdentifierNode, Node, UnaryExpressionNode } from 'src/ast';
import { Token } from 'src/tokens';
import { Result, Ok } from 'src/utils/types/Result';

import { ParserError } from '../errors/ParserError';
import { IPrefixParselet, Parser } from '../Parser';
import { Precedence } from '../Precedence';

export class UnaryParselet implements IPrefixParselet {
  public constructor(public readonly precedence = Precedence.Prefix) {}

  public parse(parser: Parser, token: Token): Result<Node, ParserError> {
    const operand = parser.parseExpression(this.precedence);

    if (Result.isErr(operand)) {
      return operand;
    }

    const identifier = new IdentifierNode(token.lexeme, token);
    return Ok(new UnaryExpressionNode(identifier, operand.value, token));
  }
}
