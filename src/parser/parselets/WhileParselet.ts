import { Node, WhileNode } from 'src/ast';
import { Token } from 'src/tokens';
import { Result, Err, Ok } from 'src/utils/types/Result';

import { ParserError } from '../errors/ParserError';
import { IPrefixParselet, Parser } from '../Parser';
import { ExpectedBlockError } from '../errors/ExpectedBlock';
import { TokenKind } from '../../tokens/TokenKind';

export class WhileParselet implements IPrefixParselet {
  public parse(parser: Parser, token: Token): Result<Node, ParserError> {
    const condition = parser.parseExpression();

    if (Result.isErr(condition)) {
      return condition;
    }

    const expectBlockOpeningResult = parser.expect(TokenKind.OpenCurly);

    if (!expectBlockOpeningResult.matches) {
      return Err(new ExpectedBlockError(expectBlockOpeningResult.token));
    }

    const body = parser.parseBlock();

    if (Result.isErr(body)) {
      return body;
    }

    return Ok(new WhileNode(condition.value, body.value, token));
  }
}
