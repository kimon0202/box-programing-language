import { Value } from './Value';
import { Type } from './Type';

export class BoxClass extends Value {
  public constructor(
    public readonly properties: Map<string, Value>,
    public readonly className: string,
  ) {
    super(Type.classObject());
  }
}
