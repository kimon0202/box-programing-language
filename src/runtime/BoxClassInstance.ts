import { Type } from './Type';
import { Value } from './Value';
import { BoxClass } from './BoxClass';

export class BoxClassInstance extends Value {
  public constructor(
    public readonly className: string,
    public readonly classReference: BoxClass,
    public readonly properties: Map<string, Value>,
  ) {
    super(Type.fromClass(className));
  }
}
