/* eslint-disable no-continue */
import { BlockNode, IVisitor } from 'src/ast';
import { Option } from 'src/utils/types/Option';

import { Type } from './Type';
import { Value } from './Value';
import { InvalidFunctionCallError } from './errors/InvalidFunctionCall';
import { BoxFunctionBody } from './builtins/base';
import { Environment } from './Environment';
import { BoxClassInstance } from './BoxClassInstance';
import { BoxValue } from './BoxValue';

type FunctionBody = BlockNode | BoxFunctionBody;

export interface BoxFunctionArgument {
  readonly name: string;
  readonly isParams: boolean;
}

export class BoxFunction extends Value {
  public readonly args: BoxFunctionArgument[];

  public constructor(
    public readonly body: FunctionBody,
    args: string[] | BoxFunctionArgument[],
    public readonly closure: Option<Environment<Value>> = Option.none(),
  ) {
    super(new Type('function'));

    this.args =
      typeof args[0] === 'string'
        ? args.map(arg => ({ name: arg as string, isParams: false }))
        : (args as BoxFunctionArgument[]);
  }

  public get arity(): number {
    return this.args.length;
  }

  public bind(instance: BoxClassInstance): BoxFunction {
    const environment = new Environment(this.closure);
    environment.set('this', instance);

    return new BoxFunction(this.body, this.args, Option.some(environment));
  }

  public call(visitor: IVisitor<Value>, ...args: Value[]) {
    if (args.length < this.arity) {
      throw new InvalidFunctionCallError(this.arity, args);
    }

    if (typeof this.body === 'function') {
      const argsMap: Record<string, Value> = {};
      for (let i = 0; i < this.args.length; i++) {
        if (i === this.args.length - 1 && this.args[i].isParams) {
          const value = new BoxValue(
            args.splice(this.args.length - 1),
            Type.array(),
          );

          argsMap[this.args[i].name] = value;
          continue;
        }

        argsMap[this.args[i].name] = args[i];
      }

      return this.body(argsMap);
    }

    const environment = new Environment<Value>(this.closure);

    for (let i = 0; i < this.args.length; i++) {
      if (i === this.args.length - 1 && this.args[i].isParams) {
        const value = new BoxValue(
          args.splice(this.args.length - 1),
          Type.array(),
        );

        environment.set(this.args[i].name, value);
        continue;
      }

      environment.set(this.args[i].name, args[i]);
    }

    return visitor.interpretFunction(this.body, environment);
  }
}
