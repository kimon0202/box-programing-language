/* eslint-disable no-nested-ternary */
import { Type } from './Type';
import { Value } from './Value';

export class BoxValue extends Value {
  public readonly value: unknown;

  public constructor(value: unknown, type: Type) {
    super(type);
    this.value = value;
  }

  public static from(value: unknown): Value {
    switch (typeof value) {
      case 'number':
        return Number.isInteger(value)
          ? new BoxValue(value, Type.integer())
          : new BoxValue(value, Type.float());
      case 'string':
        return new BoxValue(value, Type.string());
      case 'boolean':
        return new BoxValue(value, Type.boolean());
      case 'object':
        return Array.isArray(value)
          ? new BoxValue(value, Type.array())
          : new BoxValue(value, Type.object());
      case 'function':
        return new BoxValue(value, Type.function());
      default:
        // default type for all values is object
        return new BoxValue(value, Type.object());
    }
  }

  public toString(): string {
    return `${this.value} : ${this.type.toString()}`;
  }
}
