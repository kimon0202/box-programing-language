/* eslint-disable @typescript-eslint/no-non-null-assertion */

import { log } from 'src/utils/log';

import { AlreadyDefinedError } from './errors/AlreadyDefined';
import { NotDefinedError } from './errors/NotDefined';
import { Option } from '../utils/types/Option';

export class Environment<T = unknown> {
  private enclosing: Option<Environment<T>>;
  private readonly values: Map<string, T>;

  public constructor(enclosing: Option<Environment<T>> = Option.none()) {
    this.values = new Map();
    this.enclosing = enclosing;
  }

  public has(key: string): boolean {
    if (!this.values.has(key)) {
      return Option.match(this.enclosing, {
        Some: e => e.has(key),
        None: () => false,
      });
    }

    return true;
  }

  public get(key: string): T {
    if (!this.values.has(key)) {
      return Option.match(this.enclosing, {
        Some: e => e.get(key),
        None: () => {
          throw new NotDefinedError(key);
        },
      });
    }

    return this.values.get(key)!;
  }

  public set(key: string, value: T): void {
    if (this.values.has(key)) {
      throw new AlreadyDefinedError(key);
    }

    this.values.set(key, value);
  }

  public assign(key: string, value: T): void {
    if (!this.values.has(key)) {
      Option.match(this.enclosing, {
        Some: e => e.assign(key, value),
        None: () => {
          throw new NotDefinedError(key);
        },
      });
    }

    this.values.set(key, value);
  }
}
