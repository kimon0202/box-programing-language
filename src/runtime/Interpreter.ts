/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { None, Option, Some } from 'src/utils/types/Option';
import path from 'path';
import { Lexer, SourceCode } from 'src/lexer';
import { log, logError } from 'src/utils/log';

import {
  AccessNode,
  ArrayLiteralNode,
  AssignmentNode,
  BinaryExpressionNode,
  BlockNode,
  BooleanLiteralNode,
  ExportNode,
  FloatLiteralNode,
  FunctionCallNode,
  FunctionDeclarationNode,
  IdentifierNode,
  IfNode,
  ImportNode,
  IntegerLiteralNode,
  IVisitor,
  ObjectLiteralNode,
  StringLiteralNode,
  UnaryExpressionNode,
  VariableDeclarationNode,
  WhileNode,
  Node,
  IndexNode,
  AstGuards,
} from '../ast';
import { Environment } from './Environment';
import { Type } from './Type';
import { Value } from './Value';
import { BoxValue } from './BoxValue';
import { RuntimeUtils } from './utils';
import { ValueIsNotCallableError } from './errors/ValueIsNotCallable';
import { BoxFunction } from './BoxFunction';
import {
  add,
  minus,
  multiply,
  divide,
  modulo,
  equals,
  notEquals,
  not,
  and,
  or,
  less,
  lessEqual,
  greater,
  greaterEqual,
} from './builtins/operators';
import { print } from './builtins/print';
import { toString } from './builtins/toString';
import { typeOf } from './builtins/typeof';
import { CannotReadPropertyOfNonObjectValueError } from './errors/CannotReadPropertyOfNonObjectValue';
import { OnlyIntegersCanIndexArraysError } from './errors/OnlyIntegersCanIndexArrays';
import { OnlyStringsCanIndexArraysError } from './errors/OnlyStringCanIndexObjects';
import { input } from './builtins/io/input';
import { ValueIsNotExportableError } from './errors/ValueIsNotExportable';
import { BoxParser } from '../parser/BoxParser';
import { ArgumentNode, ClassDeclarationNode } from '../ast/nodes';
import { BoxClass } from './BoxClass';
import { propertiesOf } from './builtins/classes/propertiesOf';
import { CannotAssignToNonObjectValueError } from './errors/CannotAssignToNonObjectValue';
import { CannotAssignToError } from './errors/CannotAssignTo';
import { AlreadyDefinedError } from './errors/AlreadyDefined';
import { FunctionsCanOnlyContainOneParamsArgumentError } from './errors/FunctionsCanOnlyContainOneParamsArgument';
import { ParamsArgumentsMustBeTheLastDeclaredArgumentError } from './errors/ParamsArgumentsMustBeTheLastDeclaredArgument';

const STD_LIB_PATH = path.resolve(__dirname, '..', '..', 'box');

type InterpretingContext = 'Class' | 'Top-Level';

// TODO: add return expressions
// TODO: add assignment to index accessing
// TODO: add module dependency graph resolver to prevent import cycles
export class Interpreter implements IVisitor<Value> {
  private environment: Environment<Value>;
  private interpretingContext: InterpretingContext = 'Top-Level';
  private readonly sourcePath: string;

  public readonly exports: Map<string, Value>;

  public constructor(sourcePath: string) {
    this.environment = new Environment();
    this.exports = new Map();
    this.sourcePath = sourcePath;

    this.defineFunction('+', new BoxFunction(add, ['left', 'right']));
    this.defineFunction('-', new BoxFunction(minus, ['left', 'right']));
    this.defineFunction('*', new BoxFunction(multiply, ['left', 'right']));
    this.defineFunction('/', new BoxFunction(divide, ['left', 'right']));
    this.defineFunction('%', new BoxFunction(modulo, ['left', 'right']));

    this.defineFunction('==', new BoxFunction(equals, ['left', 'right']));
    this.defineFunction('!=', new BoxFunction(notEquals, ['left', 'right']));
    this.defineFunction('>', new BoxFunction(greater, ['left', 'right']));
    this.defineFunction('>=', new BoxFunction(greaterEqual, ['left', 'right']));
    this.defineFunction('<', new BoxFunction(less, ['left', 'right']));
    this.defineFunction('<=', new BoxFunction(lessEqual, ['left', 'right']));

    this.defineFunction('!', new BoxFunction(not, ['value']));
    this.defineFunction('not', new BoxFunction(not, ['value']));
    this.defineFunction('and', new BoxFunction(and, ['left', 'right']));
    this.defineFunction('or', new BoxFunction(or, ['left', 'right']));

    this.defineFunction('to-string', new BoxFunction(toString, ['value']));
    this.defineFunction('print', new BoxFunction(print, ['value']));
    this.defineFunction('type-of', new BoxFunction(typeOf, ['value']));
    this.defineFunction(
      'properties-of',
      new BoxFunction(propertiesOf, ['value']),
    );

    this.defineFunction('input', new BoxFunction(input, ['prompt']));
  }

  private defineFunction(name: string, func: BoxFunction): void {
    this.environment.set(name, func);
  }

  public interpret(nodes: Node[]): Value {
    const values = nodes.map(n => n.visit(this));
    return values[values.length - 1];
  }

  public visitIntegerLiteralNode(node: IntegerLiteralNode): Value {
    return BoxValue.from(node.value);
  }

  public visitFloatLiteralNode(node: FloatLiteralNode): Value {
    return BoxValue.from(node.value);
  }

  public visitBooleanLiteralNode(node: BooleanLiteralNode): Value {
    return BoxValue.from(node.value);
  }

  public visitStringLiteralNode(node: StringLiteralNode): Value {
    return BoxValue.from(node.value);
  }

  public visitArrayLiteralNode(node: ArrayLiteralNode): Value {
    const arr = node.values.map(n => n.visit(this));
    return BoxValue.from(arr);
  }

  public visitObjectLiteralNode(node: ObjectLiteralNode): Value {
    const record: Record<string, Value> = {};

    node.entries.forEach((valueNode, key) => {
      const value = valueNode.visit(this);
      record[key.name] = value;
    });

    return BoxValue.from(record);
  }

  public visitIdentifierNode(node: IdentifierNode): Value {
    const value = this.environment.get(node.name);
    return value;
  }

  public visitArgumentNode(node: ArgumentNode): Value {
    throw new Error('Method not implemented.');
  }

  public visitVariableDeclarationNode(node: VariableDeclarationNode): Value {
    const value = node.initializer.visit(this);

    try {
      this.environment.set(node.identifier.name, value);
    } catch (err) {
      if (err instanceof AlreadyDefinedError) {
        logError(node.token.location.toString());
      }

      throw err;
    }

    return value;
  }

  public visitAssignmentNode(node: AssignmentNode): Value {
    const value = node.value.visit(this);

    if (AstGuards.isAccessNode(node.left)) {
      const object = node.left.obj.visit(this);

      if (
        !RuntimeUtils.isObject(object) &&
        !RuntimeUtils.isBoxClassInstance(object)
      ) {
        throw new CannotAssignToNonObjectValueError('object');
      }

      if (RuntimeUtils.isObject(object)) {
        const values = object.value as Record<string, Value>;
        values[node.left.property.name] = value;
      }

      if (RuntimeUtils.isBoxClassInstance(object)) {
        object.properties.set(node.left.property.name, value);
      }

      return value;
    }

    if (AstGuards.isIdentifierNode(node.left)) {
      this.environment.assign(node.left.name, value);
      return value;
    }

    throw new CannotAssignToError(node.left);
  }

  public interpretFunction(
    node: BlockNode,
    environment: Environment<Value>,
  ): Value {
    const previousEnvironment = this.environment;
    this.environment = environment;

    const values = node.expressions.map(expr => expr.visit(this));

    this.environment = previousEnvironment;
    return values[values.length - 1];
  }

  public visitBlockNode(node: BlockNode): Value {
    const previousEnvironment = this.environment;
    this.environment = new Environment(Some(previousEnvironment));

    const values = node.expressions.map(expr => expr.visit(this));

    this.environment = previousEnvironment;
    return values[values.length - 1];
  }

  public visitFunctionDeclarationNode(node: FunctionDeclarationNode): Value {
    const paramsArguments = node.args.filter(n => n.isParams);
    if (paramsArguments.length > 1) {
      throw new FunctionsCanOnlyContainOneParamsArgumentError(
        paramsArguments.map(arg => arg.identifier.name),
      );
    }

    const paramsArgumentIndex = node.args.findIndex(arg => arg.isParams);
    if (
      paramsArgumentIndex > -1 &&
      paramsArgumentIndex !== node.args.length - 1
    ) {
      throw new ParamsArgumentsMustBeTheLastDeclaredArgumentError();
    }

    const func = new BoxFunction(
      node.body,
      node.args.map(n => ({ name: n.identifier.name, isParams: n.isParams })),
      Some(this.environment),
    );

    const identifier = Option.match(node.identifier, {
      Some: id => id.name,
      None: () => `anonymous-${Date.now()}`,
    });

    if (this.interpretingContext === 'Top-Level') {
      this.environment.set(identifier, func);
    }

    return func;
  }

  public visitFunctionCallNode(node: FunctionCallNode): Value {
    if (
      !AstGuards.isIdentifierNode(node.left) &&
      !AstGuards.isAccessNode(node.left)
    ) {
      const val = node.left.visit(this);
      throw new ValueIsNotCallableError(val);
    }

    const func = node.left.visit(this);

    if (RuntimeUtils.isBoxClass(func)) {
      const args = node.args.map(arg => arg.visit(this));
      const classInstance = RuntimeUtils.createClassInstance(func, this, args);

      return classInstance;
    }

    if (RuntimeUtils.isBoxFunction(func)) {
      const args = node.args.map(arg => arg.visit(this));
      const result = func.call(this, ...args);

      return result;
    }

    throw new ValueIsNotCallableError(func);
  }

  public visitBinaryExpressionNode(node: BinaryExpressionNode): Value {
    const func = this.environment.get(node.operator.name);

    if (!RuntimeUtils.isBoxFunction(func)) {
      throw new ValueIsNotCallableError(func);
    }

    const left = node.left.visit(this);
    const right = node.right.visit(this);

    return func.call(this, left, right) as Value;
  }

  public visitUnaryExpressionNode(node: UnaryExpressionNode): Value {
    const func = this.environment.get(node.operator.name);

    if (!RuntimeUtils.isBoxFunction(func)) {
      throw new ValueIsNotCallableError(func);
    }

    const operand = node.operand.visit(this);
    const operands =
      func.arity > 1 ? [operand, new BoxValue(null, Type.null())] : [operand];

    return func.call(this, ...operands) as Value;
  }

  public visitIfNode(node: IfNode): Value {
    const condition = node.condition.visit(this);

    if (RuntimeUtils.isTruthy(condition)) {
      return node.thenBranch.visit(this);
    }

    return Option.match(node.elseBranch, {
      Some: branch => branch.visit(this),
      None: () => new BoxValue(null, Type.null()),
    });
  }

  public visitWhileNode(node: WhileNode): Value {
    const values: Value[] = [];

    while (RuntimeUtils.isTruthy(node.condition.visit(this))) {
      const value = node.body.visit(this);
      values.push(value);
    }

    return values[values.length - 1];
  }

  public visitAccessNode(node: AccessNode): Value {
    const object = node.obj.visit(this);

    if (
      !RuntimeUtils.isObject(object) &&
      !RuntimeUtils.isBoxClassInstance(object)
    ) {
      throw new CannotReadPropertyOfNonObjectValueError(
        'object',
        node.property.name,
        object,
      );
    }

    if (RuntimeUtils.isBoxClassInstance(object)) {
      const value = object.properties.get(node.property.name);

      if (!value) {
        return new BoxValue(undefined, Type.undefined());
      }

      return value;
    }

    const values = object.value as Record<string, Value>;
    const keys = Object.keys(values);

    if (!keys.includes(node.property.name)) {
      return new BoxValue(undefined, Type.undefined());
    }

    return values[node.property.name];
  }

  public visitIndexNode(node: IndexNode): Value {
    const value = node.obj.visit(this);
    const index = node.index.visit(this);

    if (!RuntimeUtils.isArray(value) && !RuntimeUtils.isObject(value)) {
      throw new CannotReadPropertyOfNonObjectValueError(
        index.type.is(Type.string()) ? 'object' : 'array',
        (index as BoxValue).value,
        value,
      );
    }

    if (value.type.is(Type.array())) {
      const array = value.value as Value[];

      if (!RuntimeUtils.isBoxValue(index)) {
        throw new OnlyIntegersCanIndexArraysError(index);
      }

      if (!index.type.is(Type.integer())) {
        throw new OnlyIntegersCanIndexArraysError(index);
      }

      if ((index.value as number) > array.length) {
        return new BoxValue(undefined, Type.undefined());
      }

      const indexedValue = array[index.value as number];
      return indexedValue;
    }

    const values = value.value as Record<string, Value>;
    const keys = Object.keys(values);

    if (!RuntimeUtils.isBoxValue(index)) {
      throw new OnlyStringsCanIndexArraysError(index);
    }

    if (!index.type.is(Type.string())) {
      throw new OnlyStringsCanIndexArraysError(index);
    }

    if (!keys.includes(index.value as string)) {
      return new BoxValue(undefined, Type.undefined());
    }

    return values[index.value as string];
  }

  private processFile(source: SourceCode): Map<string, Value> {
    const lexer = new Lexer(source);
    const lexerResult = lexer.tokenize();

    if (lexerResult.errors.length > 0) {
      lexerResult.errors.forEach(logError);
      process.exit(1);
    }

    const parser = new BoxParser(lexerResult.tokens);
    const parserResult = parser.parse();

    if (parserResult.errors.length > 0) {
      parserResult.errors.forEach(logError);
      process.exit(1);
    }

    const interpreter = new Interpreter(source.path);
    interpreter.interpret(parserResult.nodes);

    return interpreter.exports;
  }

  public visitImportNode(node: ImportNode): Value {
    const isLocalPath = node.path.value.startsWith('.');
    const filePath = path.resolve(
      isLocalPath ? path.dirname(this.sourcePath) : STD_LIB_PATH,
      node.path.value,
    );
    const source = SourceCode.fromSync(filePath);

    const exports = this.processFile(source);

    node.identifiers.forEach(identifier => {
      const isExported = exports.has(identifier.name);

      if (!isExported) {
        this.environment.set(
          identifier.name,
          new BoxValue(undefined, Type.undefined()),
        );
        return;
      }

      this.environment.set(identifier.name, exports.get(identifier.name)!);
    });

    return new BoxValue(undefined, Type.undefined());
  }

  public visitExportNode(node: ExportNode): Value {
    if (
      !AstGuards.isVariableDeclarationNode(node.item) &&
      !AstGuards.isFunctionDeclarationNode(node.item) &&
      !AstGuards.isClassDeclarationNode(node.item)
    ) {
      throw new ValueIsNotExportableError(node.item);
    }

    const value = node.item.visit(this);

    if (AstGuards.isVariableDeclarationNode(node.item)) {
      this.exports.set(node.item.identifier.name, value);
    }

    if (AstGuards.isFunctionDeclarationNode(node.item)) {
      if (Option.isNone(node.item.identifier)) {
        throw new ValueIsNotExportableError(node.item);
      }

      this.exports.set(node.item.identifier.value.name, value);
    }

    if (AstGuards.isClassDeclarationNode(node.item)) {
      this.exports.set(node.item.name.name, value);
    }

    return value;
  }

  public visitClassDeclarationNode(node: ClassDeclarationNode): Value {
    const properties: Map<string, Value> = new Map();

    this.interpretingContext = 'Class';
    node.properties.forEach((value, key) => {
      properties.set(key.name, value.visit(this));
    });
    this.interpretingContext = 'Top-Level';

    const classValue = new BoxClass(properties, node.name.name);
    try {
      this.environment.set(node.name.name, classValue);
    } catch (err) {
      if (err instanceof AlreadyDefinedError) {
        logError(node.token.location.toString());
      }

      throw err;
    }

    return classValue;
  }
}
