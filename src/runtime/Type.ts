import { ClassDeclarationNode } from '../ast/nodes';

export class Type {
  public readonly name: string;

  public constructor(name: string) {
    this.name = name;
  }

  public toString(): string {
    return this.name;
  }

  public static anyOf(types: Type[]): Type {
    return new Type(`${types.map(t => t.name).join(' or ')}`);
  }

  public static classObject(): Type {
    return new Type('class');
  }

  public static fromClass(name: string | ClassDeclarationNode): Type {
    if (name instanceof ClassDeclarationNode) {
      return new Type(name.name.name);
    }

    return new Type(name);
  }

  public static null(): Type {
    return new Type('null');
  }

  public static integer(): Type {
    return new Type('integer');
  }

  public static float(): Type {
    return new Type('float');
  }

  public static string(): Type {
    return new Type('string');
  }

  public static boolean(): Type {
    return new Type('boolean');
  }

  public static object(): Type {
    return new Type('object');
  }

  public static array(): Type {
    return new Type('array');
  }

  public static function(): Type {
    return new Type('function');
  }

  public static undefined(): Type {
    return new Type('undefined');
  }

  public static equals(a: Type, b: Type): boolean {
    return a.equals(b);
  }

  public static is(a: Type, b: Type): boolean {
    return a.is(b);
  }

  public is(...others: Type[]): boolean {
    return others.some(other => this.equals(other));
  }

  public equals(other: Type): boolean {
    return this.name === other.name;
  }
}
