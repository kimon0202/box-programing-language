import { Type } from './Type';

export abstract class Value {
  public readonly type: Type;

  public constructor(type: Type) {
    this.type = type;
  }
}
