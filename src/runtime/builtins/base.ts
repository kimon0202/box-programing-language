import { Value } from '../Value';

export type BoxFunctionBody = (args: Record<string, Value>) => Value;
