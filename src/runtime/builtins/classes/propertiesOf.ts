import { RuntimeUtils } from 'src/runtime/utils';
import { Value } from 'src/runtime/Value';

import { BoxFunctionBody } from '../base';
import { BoxValue } from '../../BoxValue';
import { CannotReadPropertyOfNonObjectValueError } from '../../errors/CannotReadPropertyOfNonObjectValue';

export const propertiesOf: BoxFunctionBody = ({ value }) => {
  if (!RuntimeUtils.isBoxClass(value)) {
    throw new CannotReadPropertyOfNonObjectValueError(
      'object',
      'all properties',
      value,
    );
  }

  const obj: Record<string, Value> = {};
  value.properties.forEach((propertyValue, key) => {
    obj[key] = propertyValue;
  });

  return BoxValue.from(obj);
};
