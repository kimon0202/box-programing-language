import configPrompt from 'prompt-sync';
import { RuntimeUtils } from 'src/runtime/utils';
import { Type } from 'src/runtime/Type';

import { BoxFunctionBody } from '../base';
import { InvalidTypesError } from '../../errors/InvalidTypes';
import { BoxValue } from '../../BoxValue';

export const input: BoxFunctionBody = ({ prompt }) => {
  if (!RuntimeUtils.isBoxValue(prompt)) {
    throw new InvalidTypesError([Type.string()], [prompt.type]);
  }

  if (!prompt.type.is(Type.string())) {
    throw new InvalidTypesError([Type.string()], [prompt.type]);
  }

  const getInput = configPrompt();
  const result = getInput(prompt.value as string) || '';

  return BoxValue.from(result);
};
