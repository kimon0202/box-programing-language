import { Type } from '../Type';
import { InvalidTypesError } from '../errors/InvalidTypes';
import { BoxValue } from '../BoxValue';
import { BoxFunctionBody } from './base';

export const add: BoxFunctionBody = ({ left, right }) => {
  const leftValue = (left as BoxValue).value;
  const rightValue = (right as BoxValue).value;

  if (left.type.is(Type.integer())) {
    if (right.type.is(Type.float())) {
      return new BoxValue(
        (leftValue as number) + (rightValue as number),
        Type.float(),
      );
    }

    if (right.type.is(Type.integer())) {
      return new BoxValue(
        (leftValue as number) + (rightValue as number),
        Type.integer(),
      );
    }

    throw new InvalidTypesError(
      [Type.anyOf([Type.float(), Type.integer()])],
      [left.type, right.type],
    );
  }

  if (left.type.is(Type.float())) {
    if (right.type.is(Type.float())) {
      return new BoxValue(
        (leftValue as number) + (rightValue as number),
        Type.float(),
      );
    }

    if (right.type.is(Type.integer())) {
      return new BoxValue(
        (leftValue as number) + (rightValue as number),
        Type.float(),
      );
    }

    throw new InvalidTypesError(
      [Type.anyOf([Type.float(), Type.integer()])],
      [left.type, right.type],
    );
  }

  if (left.type.is(Type.string())) {
    if (right.type.is(Type.string())) {
      return new BoxValue(
        (leftValue as string) + (rightValue as string),
        Type.string(),
      );
    }

    throw new InvalidTypesError([Type.string()], [left.type, right.type]);
  }

  throw new InvalidTypesError(
    [Type.anyOf([Type.integer(), Type.float(), Type.string()])],
    [left.type, right.type],
  );
};

export const minus: BoxFunctionBody = ({ left, right }) => {
  const leftValue = (left as BoxValue).value;
  const rightValue = (right as BoxValue).value;

  if (right.type.is(Type.null())) {
    return new BoxValue(-(leftValue as number), left.type);
  }

  if (left.type.is(Type.integer())) {
    if (right.type.is(Type.float())) {
      return new BoxValue(
        (leftValue as number) - (rightValue as number),
        Type.float(),
      );
    }

    if (right.type.is(Type.integer())) {
      return new BoxValue(
        (leftValue as number) - (rightValue as number),
        Type.integer(),
      );
    }

    throw new InvalidTypesError(
      [Type.anyOf([Type.float(), Type.integer()])],
      [left.type, right.type],
    );
  }

  if (left.type.is(Type.float())) {
    if (right.type.is(Type.float())) {
      return new BoxValue(
        (leftValue as number) - (rightValue as number),
        Type.float(),
      );
    }

    if (right.type.is(Type.integer())) {
      return new BoxValue(
        (leftValue as number) - (rightValue as number),
        Type.float(),
      );
    }

    throw new InvalidTypesError(
      [Type.anyOf([Type.float(), Type.integer()])],
      [left.type, right.type],
    );
  }

  throw new InvalidTypesError(
    [Type.anyOf([Type.integer(), Type.float()])],
    [left.type, right.type],
  );
};

export const multiply: BoxFunctionBody = ({ left, right }) => {
  const leftValue = (left as BoxValue).value;
  const rightValue = (right as BoxValue).value;

  if (left.type.is(Type.integer())) {
    if (right.type.is(Type.float())) {
      return new BoxValue(
        (leftValue as number) * (rightValue as number),
        Type.float(),
      );
    }

    if (right.type.is(Type.integer())) {
      return new BoxValue(
        (leftValue as number) * (rightValue as number),
        Type.integer(),
      );
    }

    throw new InvalidTypesError(
      [Type.anyOf([Type.float(), Type.integer()])],
      [left.type, right.type],
    );
  }

  if (left.type.is(Type.float())) {
    if (right.type.is(Type.float())) {
      return new BoxValue(
        (leftValue as number) * (rightValue as number),
        Type.float(),
      );
    }

    if (right.type.is(Type.integer())) {
      return new BoxValue(
        (leftValue as number) * (rightValue as number),
        Type.float(),
      );
    }

    throw new InvalidTypesError(
      [Type.anyOf([Type.float(), Type.integer()])],
      [left.type, right.type],
    );
  }

  throw new InvalidTypesError(
    [Type.anyOf([Type.integer(), Type.float()])],
    [left.type, right.type],
  );
};

export const divide: BoxFunctionBody = ({ left, right }) => {
  const leftValue = (left as BoxValue).value;
  const rightValue = (right as BoxValue).value;

  if (left.type.is(Type.integer())) {
    if (right.type.is(Type.float())) {
      return new BoxValue(
        (leftValue as number) / (rightValue as number),
        Type.float(),
      );
    }

    if (right.type.is(Type.integer())) {
      return new BoxValue(
        (leftValue as number) / (rightValue as number),
        Type.integer(),
      );
    }

    throw new InvalidTypesError(
      [Type.anyOf([Type.float(), Type.integer()])],
      [left.type, right.type],
    );
  }

  if (left.type.is(Type.float())) {
    if (right.type.is(Type.float())) {
      return new BoxValue(
        (leftValue as number) / (rightValue as number),
        Type.float(),
      );
    }

    if (right.type.is(Type.integer())) {
      return new BoxValue(
        (leftValue as number) / (rightValue as number),
        Type.float(),
      );
    }

    throw new InvalidTypesError(
      [Type.anyOf([Type.float(), Type.integer()])],
      [left.type, right.type],
    );
  }

  throw new InvalidTypesError(
    [Type.anyOf([Type.integer(), Type.float()])],
    [left.type, right.type],
  );
};

export const modulo: BoxFunctionBody = ({ left, right }) => {
  const leftValue = (left as BoxValue).value;
  const rightValue = (right as BoxValue).value;

  if (left.type.is(Type.integer())) {
    if (right.type.is(Type.float())) {
      return new BoxValue(
        (leftValue as number) % (rightValue as number),
        Type.float(),
      );
    }

    if (right.type.is(Type.integer())) {
      return new BoxValue(
        (leftValue as number) % (rightValue as number),
        Type.integer(),
      );
    }

    throw new InvalidTypesError(
      [Type.anyOf([Type.float(), Type.integer()])],
      [left.type, right.type],
    );
  }

  if (left.type.is(Type.float())) {
    if (right.type.is(Type.float())) {
      return new BoxValue(
        (leftValue as number) % (rightValue as number),
        Type.float(),
      );
    }

    if (right.type.is(Type.integer())) {
      return new BoxValue(
        (leftValue as number) % (rightValue as number),
        Type.float(),
      );
    }

    throw new InvalidTypesError(
      [Type.anyOf([Type.float(), Type.integer()])],
      [left.type, right.type],
    );
  }

  throw new InvalidTypesError(
    [Type.anyOf([Type.integer(), Type.float()])],
    [left.type, right.type],
  );
};

export const equals: BoxFunctionBody = ({ left, right }) => {
  const leftValue = (left as BoxValue).value;
  const rightValue = (right as BoxValue).value;

  if (!left.type.is(right.type)) {
    return BoxValue.from(false);
  }

  return BoxValue.from(leftValue === rightValue);
};

export const notEquals: BoxFunctionBody = ({ left, right }) => {
  const leftValue = (left as BoxValue).value;
  const rightValue = (right as BoxValue).value;

  if (!left.type.is(right.type)) {
    return BoxValue.from(false);
  }

  return BoxValue.from(leftValue !== rightValue);
};

export const not: BoxFunctionBody = ({ value }) => {
  const valueValue = (value as BoxValue).value;
  return BoxValue.from(!valueValue);
};

export const and: BoxFunctionBody = ({ left, right }) => {
  const leftValue = (left as BoxValue).value;
  const rightValue = (right as BoxValue).value;

  if (!left.type.is(Type.boolean())) {
    throw new InvalidTypesError([Type.boolean()], [left.type]);
  }

  if (!right.type.is(Type.boolean())) {
    throw new InvalidTypesError([Type.boolean()], [right.type]);
  }

  return BoxValue.from(leftValue && rightValue);
};

export const or: BoxFunctionBody = ({ left, right }) => {
  const leftValue = (left as BoxValue).value;
  const rightValue = (right as BoxValue).value;

  if (!left.type.is(Type.boolean())) {
    throw new InvalidTypesError([Type.boolean()], [left.type]);
  }

  if (!right.type.is(Type.boolean())) {
    throw new InvalidTypesError([Type.boolean()], [right.type]);
  }

  return BoxValue.from(leftValue || rightValue);
};

const numericTypes = [Type.integer(), Type.float()];

export const less: BoxFunctionBody = ({ left, right }) => {
  if (!left.type.is(...numericTypes)) {
    throw new InvalidTypesError(
      [Type.anyOf(numericTypes)],
      [left.type, right.type],
    );
  }

  if (!right.type.is(...numericTypes)) {
    throw new InvalidTypesError(
      [Type.anyOf(numericTypes)],
      [left.type, right.type],
    );
  }

  const leftValue = (left as BoxValue).value as number;
  const rightValue = (right as BoxValue).value as number;

  return BoxValue.from(leftValue < rightValue);
};

export const lessEqual: BoxFunctionBody = ({ left, right }) => {
  if (!left.type.is(...numericTypes)) {
    throw new InvalidTypesError(
      [Type.anyOf(numericTypes)],
      [left.type, right.type],
    );
  }

  if (!right.type.is(...numericTypes)) {
    throw new InvalidTypesError(
      [Type.anyOf(numericTypes)],
      [left.type, right.type],
    );
  }

  const leftValue = (left as BoxValue).value as number;
  const rightValue = (right as BoxValue).value as number;

  return BoxValue.from(leftValue <= rightValue);
};

export const greater: BoxFunctionBody = ({ left, right }) => {
  if (!left.type.is(...numericTypes)) {
    throw new InvalidTypesError(
      [Type.anyOf(numericTypes)],
      [left.type, right.type],
    );
  }

  if (!right.type.is(...numericTypes)) {
    throw new InvalidTypesError(
      [Type.anyOf(numericTypes)],
      [left.type, right.type],
    );
  }

  const leftValue = (left as BoxValue).value as number;
  const rightValue = (right as BoxValue).value as number;

  return BoxValue.from(leftValue > rightValue);
};

export const greaterEqual: BoxFunctionBody = ({ left, right }) => {
  if (!left.type.is(...numericTypes)) {
    throw new InvalidTypesError(
      [Type.anyOf(numericTypes)],
      [left.type, right.type],
    );
  }

  if (!right.type.is(...numericTypes)) {
    throw new InvalidTypesError(
      [Type.anyOf(numericTypes)],
      [left.type, right.type],
    );
  }

  const leftValue = (left as BoxValue).value as number;
  const rightValue = (right as BoxValue).value as number;

  return BoxValue.from(leftValue >= rightValue);
};
