import { BoxValue } from '../BoxValue';
import { Type } from '../Type';
import { toString } from './toString';
import { BoxFunctionBody } from './base';

export const print: BoxFunctionBody = ({ value }) => {
  const stringValue = toString({ value }) as BoxValue;
  console.log(stringValue.value);
  return new BoxValue(null, Type.null());
};
