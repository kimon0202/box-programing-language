/* eslint-disable no-nested-ternary */
import { inspect } from 'util';

import { BoxValue } from '../BoxValue';
import { RuntimeUtils } from '../utils';
import { Value } from '../Value';
import { Type } from '../Type';
import { BoxFunctionBody } from './base';

const objToString = (obj: Record<string, unknown>): string => {
  return inspect(obj, { depth: Infinity });
};

const boxObjToJsObj = (obj: Record<string, Value>): Record<string, unknown> => {
  const jsObj: Record<string, unknown> = {};

  Object.entries(obj).forEach(([key, value]) => {
    if (RuntimeUtils.isBoxFunction(value)) {
      jsObj[key] = 'function';
    }

    if (RuntimeUtils.isBoxValue(value)) {
      if (value.type.is(Type.object())) {
        jsObj[key] = boxObjToJsObj(value.value as Record<string, Value>);
        return;
      }

      jsObj[key] = value.value;
    }
  });

  return jsObj;
};

export const toString: BoxFunctionBody = ({ value }) => {
  if (RuntimeUtils.isBoxFunction(value)) {
    return BoxValue.from('function');
  }

  if (RuntimeUtils.isBoxClass(value)) {
    return BoxValue.from('class');
  }

  if (RuntimeUtils.isBoxClassInstance(value)) {
    return BoxValue.from(`[class ${value.className}]`);
  }

  if (RuntimeUtils.isObject(value)) {
    return BoxValue.from(
      objToString(boxObjToJsObj(value.value as Record<string, Value>)),
    );
  }

  if (RuntimeUtils.isArray(value)) {
    const values = value.value as Value[];
    const string = values
      .map(v => (toString({ value: v }) as BoxValue).value)
      .join(',');

    return BoxValue.from(string);
  }

  const jsValue = (value as BoxValue).value;
  return BoxValue.from(String(jsValue));
};
