import { BoxFunctionBody } from './base';
import { BoxValue } from '../BoxValue';

export const typeOf: BoxFunctionBody = ({ value }) => {
  return BoxValue.from(value.type.name);
};
