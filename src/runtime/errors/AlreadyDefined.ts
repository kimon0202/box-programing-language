import { RuntimeError } from './RuntimeError';

export class AlreadyDefinedError extends RuntimeError {
  public constructor(name: string) {
    super(`Variable ${name} is already defined`);
  }
}
