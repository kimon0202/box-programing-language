import { Node } from 'src/ast';

import { RuntimeError } from './RuntimeError';

export class CannotAssignToError extends RuntimeError {
  public constructor(public readonly node: Node) {
    super(
      `Cannot assign value to ${node.kind}, because it isn't an assignable variable or cannot be accessed by a property or index`,
    );
  }
}
