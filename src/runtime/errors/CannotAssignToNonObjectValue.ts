import { RuntimeError } from './RuntimeError';

export class CannotAssignToNonObjectValueError extends RuntimeError {
  public constructor(public readonly type: 'array' | 'object') {
    super(
      `Cannot assign value to non-${type}, because it cannot be accessed by a property or index`,
    );
  }
}
