import { escape } from 'src/utils/strings';

import { Value } from '../Value';
import { RuntimeError } from './RuntimeError';

export class CannotReadPropertyOfNonObjectValueError extends RuntimeError {
  public constructor(
    public readonly type: 'array' | 'object',
    public readonly property: unknown,
    public readonly value: Value,
  ) {
    super(
      `Cannot read property ${escape(
        String(property),
      )} of non-${type}. Value has type ${escape(value?.type?.name)}`,
    );
  }
}
