import { RuntimeError } from './RuntimeError';

export class FunctionsCanOnlyContainOneParamsArgumentError extends RuntimeError {
  public constructor(public readonly paramsArguments: string[]) {
    super(
      `Function can only contain one params argument. Found ${
        paramsArguments.length
      }: ${paramsArguments.join(', ')}`,
    );
  }
}
