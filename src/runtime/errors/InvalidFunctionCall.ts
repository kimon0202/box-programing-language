import { Value } from '../Value';
import { RuntimeError } from './RuntimeError';

export class InvalidFunctionCallError extends RuntimeError {
  public constructor(
    public readonly arity: number,
    public readonly args: Value[],
  ) {
    super(`Function accepts ${arity} arguments, but ${args.length} were given`);
  }
}
