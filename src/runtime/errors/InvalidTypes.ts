import { Type } from '../Type';
import { RuntimeError } from './RuntimeError';

export class InvalidTypesError extends RuntimeError {
  public constructor(
    public readonly expected: Type[],
    public readonly actual: Type[],
  ) {
    super(
      `Invalid types. Expected ${expected.join(', ')}, but got ${actual.join(
        ', ',
      )}`,
    );
  }
}
