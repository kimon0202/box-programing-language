import { RuntimeError } from './RuntimeError';

export class NotDefinedError extends RuntimeError {
  public constructor(name: string) {
    super(`Variable ${name} is not defined`);
  }
}
