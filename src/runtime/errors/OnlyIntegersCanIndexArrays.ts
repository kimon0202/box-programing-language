import { Value } from '../Value';
import { RuntimeError } from './RuntimeError';

export class OnlyIntegersCanIndexArraysError extends RuntimeError {
  public constructor(got: Value) {
    super(
      `Only integers can be used as array indices, but got ${got.type.name}`,
    );
  }
}
