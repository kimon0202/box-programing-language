import { Value } from '../Value';
import { RuntimeError } from './RuntimeError';

export class OnlyStringsCanIndexArraysError extends RuntimeError {
  public constructor(got: Value) {
    super(
      `Only strings can be used as array indices, but got ${got.type.name}`,
    );
  }
}
