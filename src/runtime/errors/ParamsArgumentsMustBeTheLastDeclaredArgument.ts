import { RuntimeError } from './RuntimeError';

export class ParamsArgumentsMustBeTheLastDeclaredArgumentError extends RuntimeError {
  public constructor() {
    super('Params arguments must be the last declared argument');
  }
}
