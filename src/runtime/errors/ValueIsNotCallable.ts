import { Value } from '../Value';
import { RuntimeError } from './RuntimeError';

export class ValueIsNotCallableError extends RuntimeError {
  public constructor(public readonly value: Value) {
    super(`${value} is not callable`);
  }
}
