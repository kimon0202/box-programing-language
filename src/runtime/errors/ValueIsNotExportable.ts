import { Node } from 'src/ast';

import { RuntimeError } from './RuntimeError';

export class ValueIsNotExportableError extends RuntimeError {
  public constructor(public readonly node: Node) {
    super(
      `Only variables, named function declarations and classes are exportable. Got ${node.kind}`,
    );
  }
}
