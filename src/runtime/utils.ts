/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { BlockNode, IVisitor } from 'src/ast';

import { BoxFunction } from './BoxFunction';
import { Value } from './Value';
import { BoxValue } from './BoxValue';
import { Type } from './Type';
import { BoxClass } from './BoxClass';
import { BoxClassInstance } from './BoxClassInstance';
import { ValueIsNotCallableError } from './errors/ValueIsNotCallable';

const isBoxFunction = (value: Value): value is BoxFunction =>
  value instanceof BoxFunction;

const isBoxValue = (value: Value): value is BoxValue =>
  value instanceof BoxValue;

const isBoxClass = (value: Value): value is BoxClass =>
  value instanceof BoxClass;

const isBoxClassInstance = (value: Value): value is BoxClassInstance =>
  value instanceof BoxClassInstance;

const isObject = (value: Value): value is BoxValue =>
  isBoxValue(value) && value.type.is(Type.object());

const isArray = (value: Value): value is BoxValue =>
  isBoxValue(value) && value.type.is(Type.array());

const equals = (a: Value, b: Value): boolean => {
  if (!a.type.equals(b.type)) {
    return false;
  }

  return (
    (a as unknown as { value: unknown }).value ===
    (b as unknown as { value: unknown }).value
  );
};

const createClassInstance = (
  boxClass: BoxClass,
  visitor: IVisitor<Value>,
  args: Value[],
): BoxClassInstance => {
  const properties = new Map<string, Value>();

  const instance = new BoxClassInstance(
    boxClass.className,
    boxClass,
    properties,
  );

  boxClass.properties.forEach((value, key) => {
    const func = value as BoxFunction;
    properties.set(key, func.bind(instance));
  });

  if (properties.has(boxClass.className)) {
    const constructor = properties.get(boxClass.className) as BoxFunction;
    constructor.call(visitor, ...args);
  }

  return instance;
};

const isTruthy = (a: Value): boolean => {
  if (a.type.is(Type.boolean())) {
    return (a as BoxValue).value as boolean;
  }

  if (a.type.is(Type.null()) || a.type.is(Type.undefined())) {
    return false;
  }

  return true;
};

export const RuntimeUtils = {
  isBoxFunction,
  isBoxValue,
  isBoxClass,
  isBoxClassInstance,
  isObject,
  isArray,
  equals,
  isTruthy,
  createClassInstance,
};
