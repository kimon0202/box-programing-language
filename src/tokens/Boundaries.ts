import { TokenKind } from './TokenKind';

export const boundaries = Array.from(
  new Set<TokenKind>([
    TokenKind.If,
    TokenKind.While,
    TokenKind.Fun,
    TokenKind.Export,
    TokenKind.Import,
    TokenKind.Let,
    TokenKind.Type,
    TokenKind.Enum,
    TokenKind.Struct,
  ]),
);
