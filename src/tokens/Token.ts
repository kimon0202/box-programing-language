import { Location } from '../utils/Location';
import { escape } from '../utils/strings';
import { TokenKind } from './TokenKind';

export class Token {
  public readonly kind: TokenKind;
  public readonly lexeme: string;
  public readonly location: Location;

  public constructor(kind: TokenKind, lexeme: string, location: Location) {
    this.kind = kind;
    this.lexeme = lexeme;
    this.location = location;
  }

  public is(...kinds: TokenKind[]): boolean {
    return kinds.includes(this.kind);
  }

  public toString(): string {
    return `Token(${this.kind}, ${escape(this.lexeme)})`;
  }
}
