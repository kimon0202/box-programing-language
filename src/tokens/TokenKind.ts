export type TokenOpenParen = 'OpenParen';
export type TokenCloseParen = 'CloseParen';
export type TokenOpenBracket = 'OpenBracket';
export type TokenCloseBracket = 'CloseBracket';
export type TokenOpenCurly = 'OpenCurly';
export type TokenCloseCurly = 'CloseCurly';
export type TokenColon = 'Colon';
export type TokenDot = 'Dot';
export type TokenComma = 'Comma';

export type TokenPlus = 'Plus';
export type TokenMinus = 'Minus';
export type TokenStar = 'Star';
export type TokenSlash = 'Slash';
export type TokenModulo = 'Modulo';
export type TokenGreater = 'Greater';
export type TokenLess = 'Less';
export type TokenEqual = 'Equal';
export type TokenLessEqual = 'LessEqual';
export type TokenGreaterEqual = 'GreaterEqual';
export type TokenEqualEqual = 'EqualEqual';
export type TokenBang = 'Bang';
export type TokenBangEqual = 'BangEqual';
export type TokenPipe = 'Pipe';

export type TokenArrow = 'Arrow';
export type TokenPercentCurly = 'PercentCurly';
export type TokenColonColon = 'ColonColon';
export type TokenDotDot = 'DotDot';

export type TokenIf = 'If';
export type TokenElse = 'Else';
export type TokenExtends = 'Extends';
export type TokenWhile = 'While';
export type TokenDo = 'Do';
export type TokenFun = 'Fun';
export type TokenImport = 'Import';
export type TokenExport = 'Export';
export type TokenModule = 'Module';
export type TokenTrue = 'True';
export type TokenFalse = 'False';
export type TokenLet = 'Let';
export type TokenIn = 'In';
export type TokenAs = 'As';
export type TokenAnd = 'And';
export type TokenOr = 'Or';
export type TokenNot = 'Not';
export type TokenMut = 'Mut';
export type TokenType = 'Type';
export type TokenEnum = 'Enum';
export type TokenStruct = 'Struct';
export type TokenFrom = 'From';
export type TokenClass = 'Class';
export type TokenParams = 'Params';

export type TokenString = 'String';
export type TokenInteger = 'Integer';
export type TokenFloat = 'Float';
export type TokenIdentifier = 'Identifier';

export type TokenError = 'Error';
export type TokenEof = 'Eof';

export type TokenKind =
  | TokenOpenParen
  | TokenCloseParen
  | TokenOpenBracket
  | TokenCloseBracket
  | TokenOpenCurly
  | TokenCloseCurly
  | TokenColon
  | TokenDot
  | TokenComma
  | TokenPlus
  | TokenMinus
  | TokenStar
  | TokenSlash
  | TokenModulo
  | TokenGreater
  | TokenLess
  | TokenEqual
  | TokenLessEqual
  | TokenGreaterEqual
  | TokenEqualEqual
  | TokenBang
  | TokenBangEqual
  | TokenPipe
  | TokenArrow
  | TokenPercentCurly
  | TokenColonColon
  | TokenDotDot
  | TokenIf
  | TokenElse
  | TokenExtends
  | TokenWhile
  | TokenDo
  | TokenFun
  | TokenImport
  | TokenExport
  | TokenModule
  | TokenTrue
  | TokenFalse
  | TokenLet
  | TokenIn
  | TokenAs
  | TokenAnd
  | TokenOr
  | TokenNot
  | TokenMut
  | TokenType
  | TokenEnum
  | TokenStruct
  | TokenFrom
  | TokenClass
  | TokenParams
  | TokenString
  | TokenInteger
  | TokenFloat
  | TokenIdentifier
  | TokenError
  | TokenEof;

const OpenParen: TokenOpenParen = 'OpenParen';
const CloseParen: TokenCloseParen = 'CloseParen';
const OpenBracket: TokenOpenBracket = 'OpenBracket';
const CloseBracket: TokenCloseBracket = 'CloseBracket';
const OpenCurly: TokenOpenCurly = 'OpenCurly';
const CloseCurly: TokenCloseCurly = 'CloseCurly';
const Colon: TokenColon = 'Colon';
const Dot: TokenDot = 'Dot';
const Comma: TokenComma = 'Comma';

const Plus: TokenPlus = 'Plus';
const Minus: TokenMinus = 'Minus';
const Star: TokenStar = 'Star';
const Slash: TokenSlash = 'Slash';
const Modulo: TokenModulo = 'Modulo';
const Greater: TokenGreater = 'Greater';
const Less: TokenLess = 'Less';
const Equal: TokenEqual = 'Equal';
const LessEqual: TokenLessEqual = 'LessEqual';
const GreaterEqual: TokenGreaterEqual = 'GreaterEqual';
const EqualEqual: TokenEqualEqual = 'EqualEqual';
const Bang: TokenBang = 'Bang';
const BangEqual: TokenBangEqual = 'BangEqual';
const Pipe: TokenPipe = 'Pipe';

const Arrow: TokenArrow = 'Arrow';
const PercentCurly: TokenPercentCurly = 'PercentCurly';
const ColonColon: TokenColonColon = 'ColonColon';
const DotDot: TokenDotDot = 'DotDot';

const If: TokenIf = 'If';
const Else: TokenElse = 'Else';
const Extends: TokenExtends = 'Extends';
const While: TokenWhile = 'While';
const Do: TokenDo = 'Do';
const Fun: TokenFun = 'Fun';
const Import: TokenImport = 'Import';
const Export: TokenExport = 'Export';
const Module: TokenModule = 'Module';
const True: TokenTrue = 'True';
const False: TokenFalse = 'False';
const Let: TokenLet = 'Let';
const In: TokenIn = 'In';
const As: TokenAs = 'As';
const And: TokenAnd = 'And';
const Or: TokenOr = 'Or';
const Not: TokenNot = 'Not';
const Mut: TokenMut = 'Mut';
const Type: TokenType = 'Type';
const Enum: TokenEnum = 'Enum';
const Struct: TokenStruct = 'Struct';
const From: TokenFrom = 'From';
const Class: TokenClass = 'Class';
const Params: TokenParams = 'Params';

const String: TokenString = 'String';
const Integer: TokenInteger = 'Integer';
const Float: TokenFloat = 'Float';
const Identifier: TokenIdentifier = 'Identifier';

const Error: TokenError = 'Error';
const Eof: TokenEof = 'Eof';

export const TokenKind = {
  OpenParen,
  CloseParen,
  OpenBracket,
  CloseBracket,
  OpenCurly,
  CloseCurly,
  Colon,
  Dot,
  Comma,
  Plus,
  Minus,
  Star,
  Slash,
  Modulo,
  Greater,
  Less,
  Equal,
  LessEqual,
  GreaterEqual,
  EqualEqual,
  Bang,
  BangEqual,
  Pipe,
  Arrow,
  PercentCurly,
  ColonColon,
  DotDot,
  If,
  Else,
  Extends,
  While,
  Do,
  Fun,
  Import,
  Export,
  Module,
  True,
  False,
  Let,
  In,
  As,
  And,
  Or,
  Not,
  Mut,
  Type,
  Enum,
  Struct,
  From,
  Class,
  Params,
  String,
  Integer,
  Float,
  Identifier,
  Error,
  Eof,
};
