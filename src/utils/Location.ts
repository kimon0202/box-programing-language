export interface Span {
  readonly start: number;
  readonly end: number;
}

export class Location {
  public readonly file: string;
  public readonly line: number;
  public readonly column: Span;
  public readonly sourceSpan: Span;

  public constructor(
    file: string,
    line: number,
    column: Span,
    sourceSpan: Span,
  ) {
    this.file = file;
    this.line = line;
    this.column = column;
    this.sourceSpan = sourceSpan;
  }

  public toString(): string {
    return `${this.file}:${this.line}:${this.column.start}`;
  }
}
