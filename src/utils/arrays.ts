export const findLastIndex = <Value>(
  arr: Value[],
  predicate: (item: Value) => boolean,
): number => {
  let index = -1;
  for (let i = arr.length - 1; i >= 0; i--) {
    if (predicate(arr[i])) {
      index = i;
      break;
    }
  }

  return index;
};

export const findFirstAfterIndex = <Value>(
  arr: Value[],
  index: number,
  predicate: (value: Value) => boolean,
): number => {
  for (let i = index; i < arr.length; i++) {
    if (predicate(arr[i])) {
      return i;
    }
  }

  return -1;
};
