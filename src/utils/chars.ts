import { Option } from './types/Option';

const symbols = [
  '+',
  '-',
  '*',
  '/',
  '%',
  '^',
  '=',
  '<',
  '>',
  '!',
  '&',
  '|',
  '~',
  '?',
];

export const Char = {
  isWhitespace(char: string | Option<string>): boolean {
    if (typeof char === 'object') {
      return Option.match(char, {
        Some: c => /\s/.test(c),
        None: () => false,
      });
    }

    return /\s/.test(char);
  },

  isDigit(char: string | Option<string>): boolean {
    if (typeof char === 'object') {
      return Option.match(char, {
        Some: c => c >= '0' && c <= '9',
        None: () => false,
      });
    }

    return char >= '0' && char <= '9';
  },

  isAlpha(char: string | Option<string>): boolean {
    if (typeof char === 'object') {
      return Option.match(char, {
        Some: c =>
          (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c === '_',
        None: () => false,
      });
    }

    return (
      (char >= 'a' && char <= 'z') ||
      (char >= 'A' && char <= 'Z') ||
      char === '_'
    );
  },

  isSymbol(char: string | Option<string>): boolean {
    if (typeof char === 'object') {
      return Option.match(char, {
        Some: c => symbols.includes(c),
        None: () => false,
      });
    }

    return symbols.includes(char);
  },

  isAlphaNumeric(char: string | Option<string>): boolean {
    return this.isAlpha(char) || this.isDigit(char);
  },
};
