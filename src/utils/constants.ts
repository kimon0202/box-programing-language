import path from 'path';

export const RUNTIME_BOX_STD_PATH = path.resolve(__dirname, '..', '..', 'box');
export const RUNTIME_EXPORT_IDENTIFIER = '__exports__';
