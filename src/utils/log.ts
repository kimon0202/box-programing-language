import { inspect } from 'util';

export const log = (...items: unknown[]): void => {
  const final = items.map(i => inspect(i, { depth: Infinity, colors: true }));
  console.log(...final);
};

export const logError = (error: Error | string): void => {
  const final = `\x1b[31m[ERROR]\x1b[0m ${inspect(error, {
    depth: Infinity,
    colors: true,
  })}`;

  console.error(final);
};
