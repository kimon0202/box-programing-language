export const escape = (value: string): string => `\`${value}\``;
