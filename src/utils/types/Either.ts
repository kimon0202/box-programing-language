type LeftTag = '_left';
type RightTag = '_right';

type TLeft<Value> = { readonly tag: LeftTag; readonly value: Value };
type TRight<Value> = { readonly tag: RightTag; readonly value: Value };

export type EitherIsRight<E extends Either<never, never>> =
  E['tag'] extends RightTag ? true : false;

export type EitherIsLeft<E extends Either<never, never>> =
  E['tag'] extends LeftTag ? true : false;

type GetLeftOrElseCallback<Left, Right> = (right: Right) => Left;
type GetRightOrElseCallback<Left, Right> = (left: Left) => Right;

export type Either<Left, Right> = TLeft<Left> | TRight<Right>;

const left = <Left = never, Right = never>(
  value: Left,
): Either<Left, Right> => ({
  tag: '_left',
  value,
});

const right = <Left = never, Right = never>(
  value: Right,
): Either<Left, Right> => ({
  tag: '_right',
  value,
});

const isLeft = <Left = never, Right = never>(
  e: Either<Left, Right>,
): e is TLeft<Left> => {
  return e.tag === '_left';
};

const isRight = <Left = never, Right = never>(
  e: Either<Left, Right>,
): e is TRight<Right> => {
  return e.tag === '_right';
};

const getLeftOrElse = <Left, Right>(
  e: Either<Left, Right>,
  callback: GetLeftOrElseCallback<Left, Right>,
): Left => {
  if (isLeft(e)) {
    return e.value;
  }

  return callback(e.value);
};

const getRightOrElse = <Left, Right>(
  e: Either<Left, Right>,
  callback: GetRightOrElseCallback<Left, Right>,
): Right => {
  if (isRight(e)) {
    return e.value;
  }

  return callback(e.value);
};

export const Either = {
  left,
  right,
  isLeft,
  isRight,
  getLeftOrElse,
  getRightOrElse,
};
