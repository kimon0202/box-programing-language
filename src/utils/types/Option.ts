type SomeTag = '_some';
type NoneTag = '_none';

type TSome<Value> = { readonly tag: SomeTag; readonly value: Value };
type TNone = { readonly tag: NoneTag };

type GetOrElseCallback<Value> = () => Value;
type MapCallback<Value, Mapped> = (value: Value) => Mapped;

type MatchFunction = <Value, Return>(
  option: Option<Value>,
  branches: {
    None: () => Return;
    Some: (value: Value) => Return;
  },
) => Return;

export type Option<Value> = TSome<Value> | TNone;

const some = <Value>(value: Value): Option<Value> => ({ tag: '_some', value });
const none = <Value = never>(): Option<Value> => ({ tag: '_none' });

export const Some = some;
export const None = none();

const isSome = <Value = never>(
  option: Option<Value>,
): option is TSome<Value> => {
  return option.tag === '_some';
};

const isNone = <Value = never>(option: Option<Value>): option is TNone => {
  return option.tag === '_none';
};

const expect = <Value = never>(
  option: Option<Value>,
  message: string,
): Value => {
  if (isSome(option)) {
    return option.value;
  }

  throw new Error(message);
};

const get = <Value = never>(option: Option<Value>): Value => {
  if (isSome(option)) {
    return option.value;
  }

  throw new Error('Unable to unwrap option because it is None');
};

const getOr = <Value = never>(option: Option<Value>, value: Value): Value => {
  if (isSome(option)) {
    return option.value;
  }

  return value;
};

const getOrElse = <Value = never>(
  option: Option<Value>,
  onNone: GetOrElseCallback<Value>,
): Value => {
  if (isSome(option)) {
    return option.value;
  }

  return onNone();
};

const map = <Value = never, MappedValue = never>(
  option: Option<Value>,
  func: MapCallback<Value, MappedValue>,
): Option<MappedValue> => {
  if (isSome(option)) {
    return some(func(option.value));
  }

  return none();
};

const mapOr = <Value = never, MappedValue = never>(
  option: Option<Value>,
  func: MapCallback<Value, MappedValue>,
  value: MappedValue,
): MappedValue => {
  if (isSome(option)) {
    return func(option.value);
  }

  return value;
};

const mapOrElse = <Value = never, MappedValue = never>(
  option: Option<Value>,
  onSome: MapCallback<Value, MappedValue>,
  onNone: () => MappedValue,
): MappedValue => {
  if (isSome(option)) {
    return onSome(option.value);
  }

  return onNone();
};

const match: MatchFunction = (option, branches) => {
  if (isSome(option)) {
    return branches.Some(option.value);
  }

  return branches.None();
};

const isFunction = <Value>(
  v: Value | ((a: Value) => boolean),
): v is (a: Value) => boolean => {
  return typeof v === 'function';
};

const is = <Value>(
  option: Option<Value>,
  expected: Value | ((v: Value) => boolean),
): boolean => {
  if (isSome(option)) {
    if (isFunction(expected)) {
      return expected(option.value);
    }

    return option.value === expected;
  }

  return false;
};

export const Option = {
  some,
  none,
  isSome,
  isNone,
  expect,
  get,
  getOr,
  getOrElse,
  map,
  mapOr,
  mapOrElse,
  match,
  is,
};
