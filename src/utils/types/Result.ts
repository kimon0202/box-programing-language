import { Option } from './Option';

type ErrorTag = '_error';
type ValueTag = '_value';

type TError<Error> = { readonly tag: ErrorTag; readonly value: Error };
type TValue<Value> = { readonly tag: ValueTag; readonly value: Value };

export type Result<Value, Error> = TValue<Value> | TError<Error>;

type MapFunction<V, M> = (value: V) => M;
type Match = <V = never, E = Error, R = V>(
  result: Result<V, E>,
  branches: {
    Ok: (value: V) => R;
    Error: (error: E) => R;
  },
) => R;

const ok = <V = never, E = Error>(value: V): Result<V, E> => ({
  tag: '_value',
  value,
});

const err = <V = never, E = Error>(error: E): Result<V, E> => ({
  tag: '_error',
  value: error,
});

export const Ok = ok;
export const Err = err;

const isOk = <V = never, E = Error>(
  value: Result<V, E>,
): value is TValue<V> => {
  return value.tag === '_value';
};

const isErr = <V = never, E = Error>(
  value: Result<V, E>,
): value is TError<E> => {
  return value.tag === '_error';
};

const toOkOption = <V = never, E = Error>(result: Result<V, E>): Option<V> => {
  if (isErr(result)) {
    return Option.none();
  }

  return Option.some(result.value);
};

const toErrOption = <V = never, E = Error>(result: Result<V, E>): Option<E> => {
  if (isOk(result)) {
    return Option.none();
  }

  return Option.some(result.value);
};

const map = <V = never, E = Error, M = never>(
  result: Result<V, E>,
  func: MapFunction<V, M>,
): Result<M, E> => {
  if (isOk(result)) {
    return ok(func(result.value));
  }

  return err(result.value);
};

const mapOr = <V = never, E = Error, M = never>(
  result: Result<V, E>,
  def: M,
  onOk: (value: V) => M,
): M => {
  if (isOk(result)) {
    return onOk(result.value);
  }

  return def;
};

const mapOrElse = <V = never, E = Error, M = never>(
  result: Result<V, E>,
  onErr: (error: E) => M,
  onOk: (value: V) => M,
) => {
  if (isOk(result)) {
    return onOk(result.value);
  }

  return onErr(result.value);
};

const expect = <V = never, E = Error>(
  result: Result<V, E>,
  message: string,
): V => {
  if (isOk(result)) {
    return result.value;
  }

  throw new Error(message);
};

const getOr = <V = never, E = Error>(result: Result<V, E>, def: V): V => {
  if (isOk(result)) {
    return result.value;
  }

  return def;
};

const getOrElse = <V = never, E = Error>(
  result: Result<V, E>,
  onErr: (error: E) => V,
): V => {
  if (isOk(result)) {
    return result.value;
  }

  return onErr(result.value);
};

const match: Match = (result, branches) => {
  if (isOk(result)) {
    return branches.Ok(result.value);
  }

  return branches.Error(result.value);
};

export const Result = {
  ok,
  err,
  isOk,
  isErr,
  toOkOption,
  toErrOption,
  map,
  mapOr,
  mapOrElse,
  expect,
  getOr,
  getOrElse,
  match,
};
