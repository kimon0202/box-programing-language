import { Lexer, SourceCode } from '../src/lexer';
import { UnknownEscapeSequenceError } from '../src/lexer/errors/UnknownEscapeSequence';
import { UnterminatedStringError } from '../src/lexer/errors/UnterminatedString';
import { Keywords as KeywordsMap } from '../src/lexer/keywords';
import { TokenKind } from '../src/tokens';

const keywords = Array.from(KeywordsMap.entries());

describe('Lexer', () => {
  it('should tokenize all language keywords', () => {
    const source: SourceCode = {
      content: keywords.map(([keyword]) => keyword).join(' '),
      path: 'TESTS',
    };

    const lexer = new Lexer(source);
    const { tokens } = lexer.tokenize();

    expect(tokens.length).toBe(keywords.length + 1);
    tokens.forEach((token, index) => {
      if (index === tokens.length - 1) {
        expect(token.kind).toBe(TokenKind.Eof);
        return;
      }

      expect(token.lexeme).toBe(keywords[index][0]);
      expect(token.kind).toBe(keywords[index][1]);
    });
  });

  it('should tokenize all symbols', () => {
    const source: SourceCode = {
      content: '()[]{}:.,+-*/%>< = <= >= == != ! | -> %{ :: ..',
      path: 'TESTS',
    };

    const lexer = new Lexer(source);
    const { tokens } = lexer.tokenize();

    expect(tokens.length).toBe(28);
    expect(tokens.map(token => token.kind)).toStrictEqual([
      TokenKind.OpenParen,
      TokenKind.CloseParen,
      TokenKind.OpenBracket,
      TokenKind.CloseBracket,
      TokenKind.OpenCurly,
      TokenKind.CloseCurly,
      TokenKind.Colon,
      TokenKind.Dot,
      TokenKind.Comma,
      TokenKind.Plus,
      TokenKind.Minus,
      TokenKind.Star,
      TokenKind.Slash,
      TokenKind.Modulo,
      TokenKind.Greater,
      TokenKind.Less,
      TokenKind.Equal,
      TokenKind.LessEqual,
      TokenKind.GreaterEqual,
      TokenKind.EqualEqual,
      TokenKind.BangEqual,
      TokenKind.Bang,
      TokenKind.Pipe,
      TokenKind.Arrow,
      TokenKind.PercentCurly,
      TokenKind.ColonColon,
      TokenKind.DotDot,
      TokenKind.Eof,
    ]);
  });

  it('should tokenize custom identifiers', () => {
    const source: SourceCode = {
      content: 'foo BAR baz-something _underscore middle_under9483score',
      path: 'TESTS',
    };

    const lexer = new Lexer(source);
    const { tokens } = lexer.tokenize();

    expect(tokens.length).toBe(6);
    expect(tokens.slice(0, 5).every(t => t.kind === TokenKind.Identifier)).toBe(
      true,
    );

    expect(tokens[0].lexeme).toBe('foo');
    expect(tokens[1].lexeme).toBe('BAR');
    expect(tokens[2].lexeme).toBe('baz-something');
    expect(tokens[3].lexeme).toBe('_underscore');
    expect(tokens[4].lexeme).toBe('middle_under9483score');

    expect(tokens[5].kind).toBe(TokenKind.Eof);
  });

  it('should ignore line comments', () => {
    const source: SourceCode = {
      content: '// foo',
      path: 'TESTS',
    };

    const lexer = new Lexer(source);
    const { tokens } = lexer.tokenize();

    expect(tokens.length).toBe(1);
    expect(tokens[0].kind).toBe(TokenKind.Eof);
  });

  describe('Numbers', () => {
    it('should tokenize integers', () => {
      const source: SourceCode = {
        content: '1234567890 48930 34020984',
        path: 'TESTS',
      };

      const lexer = new Lexer(source);
      const { tokens } = lexer.tokenize();

      expect(tokens.length).toBe(4);

      expect(tokens[0].kind).toBe(TokenKind.Integer);
      expect(tokens[0].lexeme).toBe('1234567890');

      expect(tokens[1].kind).toBe(TokenKind.Integer);
      expect(tokens[1].lexeme).toBe('48930');

      expect(tokens[2].kind).toBe(TokenKind.Integer);
      expect(tokens[2].lexeme).toBe('34020984');

      expect(tokens[3].kind).toBe(TokenKind.Eof);
    });

    it('should tokenize floats', () => {
      const source: SourceCode = {
        content: '123.456 789.0 0.123',
        path: 'TESTS',
      };

      const lexer = new Lexer(source);
      const { tokens } = lexer.tokenize();

      expect(tokens.length).toBe(4);

      expect(tokens[0].kind).toBe(TokenKind.Float);
      expect(tokens[0].lexeme).toBe('123.456');

      expect(tokens[1].kind).toBe(TokenKind.Float);
      expect(tokens[1].lexeme).toBe('789.0');

      expect(tokens[2].kind).toBe(TokenKind.Float);
      expect(tokens[2].lexeme).toBe('0.123');

      expect(tokens[3].kind).toBe(TokenKind.Eof);
    });

    it('should tokenize numbers with _ as separators', () => {
      const source: SourceCode = {
        content: '123_456_789_0_123',
        path: 'TESTS',
      };

      const lexer = new Lexer(source);
      const { tokens } = lexer.tokenize();

      expect(tokens.length).toBe(2);

      expect(tokens[0].kind).toBe(TokenKind.Integer);
      expect(tokens[0].lexeme).toBe('1234567890123');

      expect(tokens[1].kind).toBe(TokenKind.Eof);
    });
  });

  describe('Strings', () => {
    it('should tokenize strings', () => {
      const source: SourceCode = {
        content: "'foz' 'Hello, World'",
        path: 'TESTS',
      };

      const lexer = new Lexer(source);
      const { tokens } = lexer.tokenize();

      expect(tokens.length).toBe(3);

      expect(tokens[0].kind).toBe(TokenKind.String);
      expect(tokens[0].lexeme).toBe('foz');

      expect(tokens[1].kind).toBe(TokenKind.String);
      expect(tokens[1].lexeme).toBe('Hello, World');

      expect(tokens[2].kind).toBe(TokenKind.Eof);
    });

    it('should return an error if a string is not terminated correctly', () => {
      const source: SourceCode = {
        content: "'foz' 'Hello, World",
        path: 'TESTS',
      };

      const lexer = new Lexer(source);
      const { tokens, errors } = lexer.tokenize();

      expect(tokens.length).toBe(2);
      expect(errors.length).toBe(1);

      expect(tokens[0].kind).toBe(TokenKind.String);
      expect(tokens[0].lexeme).toBe('foz');

      expect(tokens[1].kind).toBe(TokenKind.Error);
      expect(errors[0]).toBeInstanceOf(UnterminatedStringError);
    });

    it('should tokenize strings with escape sequences', () => {
      const source: SourceCode = {
        // eslint-disable-next-line prettier/prettier, no-useless-escape
        content: "'\\\\ \\' \\0 \\a \\b \\f \\n \\r \\t \\v'",
        path: 'TESTS',
      };

      const lexer = new Lexer(source);
      const { tokens } = lexer.tokenize();

      expect(tokens.length).toBe(2);

      expect(tokens[0].kind).toBe(TokenKind.String);
      // eslint-disable-next-line prettier/prettier, no-useless-escape
      expect(tokens[0].lexeme).toBe("\\ ' \0 \a \b \f \n \r \t \v");

      expect(tokens[1].kind).toBe(TokenKind.Eof);
    });

    it('should return an error if a unknown escape sequence is used', () => {
      const source: SourceCode = {
        content: "'\\z'",
        path: 'TESTS',
      };

      const lexer = new Lexer(source);
      const { tokens, errors } = lexer.tokenize();

      expect(tokens.length).toBe(2);
      expect(errors.length).toBe(1);

      expect(tokens[0].kind).toBe(TokenKind.Error);
      expect(tokens[1].kind).toBe(TokenKind.Eof);

      expect(errors[0]).toBeInstanceOf(UnknownEscapeSequenceError);
    });
  });
});
